<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/prolog.php");

if(!$USER->IsAdmin()) return;
CModule::includeModule('pgsoft.dynamicontentredactor');

print_r($_REQUEST['delete_id']);

switch ($_REQUEST['action']){
    case 'getprop' : echo(json_encode(\Dbe\DbeFormEdit::getIblockProps($_REQUEST['iblock_id'])));
    case 'setcontent' : echo(json_encode(\Dbe\DynamicEditorBlockContent::saveContent(
        $_REQUEST['iblock_id'],
        $_REQUEST['iblock_element_id'],
        $_REQUEST['iblock_field'],
        $_REQUEST['iblock_prop'],
    )));
    case 'cleancontent' && $_REQUEST['delete_id'] : echo(json_encode(\Dbe\DynamicEditorBlockContent::cleanContent(
        $_REQUEST['page'],
        $_REQUEST['step'],
        $_REQUEST['delete_id'],
    )));
    case 'cleancontent' : echo(json_encode(\Dbe\DynamicEditorBlockContent::cleanContent(
        $_REQUEST['page'],
        $_REQUEST['step'],
    )));
    break;

}

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin_after.php");