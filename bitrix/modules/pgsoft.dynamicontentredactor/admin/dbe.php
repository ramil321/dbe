<?php
error_reporting(E_ALL);
ini_set('display_errors','On');
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/prolog.php");

if($_REQUEST['page'] == 'list'){
    include 'dbe_list.php';
}elseif($_REQUEST['page'] == 'element'){
    include 'dbe_element.php';
}else{
    include 'dbe_list.php';
}
