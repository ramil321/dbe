<?php

namespace  Dbe;
use Bitrix\Main\Entity\ExpressionField;

\CModule::IncludeModule('iblock');

class DynamicEditorBlockContent
{
    public static function saveContent($iblockId, $elementId, $iblockFiled, $iblockProp)
    {
        $params = [
            'iblock_id' => $iblockId,
            'iblock_element_id' => $elementId,
            'iblock_filed' => $iblockFiled,
            'iblock_prop' => $iblockProp,
        ];
        $result = \Dbe\Tables\DbeContentTable::add($params);
        if (!$result->isSuccess())
        {
            $errors = $result->getErrorMessages();
        }
        return 'ok';
    }

    public static function cleanContent($page, $step, $deleteId = null)
    {

        $filter = array();
        if ($deleteId) {
            $filter['id'] = $deleteId;
        }

        $curCount = \Dbe\Tables\DbeContentTable::getList(array(
                'select' => array('CNT'),
                'filter' => $filter,
                'runtime' => array(
                    new ExpressionField('CNT', 'COUNT(*)')
                )
            ))->fetch();
        if($page == 1) {
            $allCount = $curCount;
        }
        $contentList = \Dbe\Tables\DbeContentTable::getList(
            array(
                'select' => array('*'),
                'limit' => $step,
            )
        )->fetchAll();

        $convertContentFieldsIds = [];
        $convertContentPropsIds = [];
        foreach ($contentList as $content) {
            $convertContentFieldsIds[$content['iblock_id']][] =  $content['iblock_element_id'];
            $convertContentPropsIds[$content['iblock_id']][$content['iblock_element_id']][$content['iblock_prop_id']] =  $content['iblock_prop_id'];
        }
        foreach ($convertContentFieldsIds as $iblockId=>$elementIds) {
            self::getIblockContentConvertAndSave(
                $iblockId,
                $elementIds
            );
            self::getIblockContentPropsConvertAndSave(
                $iblockId,
                $elementIds,
                $convertContentPropsIds
            );
        }

        return array(
            'sizeof' => '',
            'memory'=>'',
            'all'=> isset($allCount['CNT']) ? (int) $allCount['CNT'] : '',
            'cur_count'=> (int) $curCount['CNT'],
            'page' => (int) $page + 1
        );

    }

    private static function getIblockContentConvertAndSave($iblockId, $elementIds)
    {
        $arSelect = Array("ID", "PREVIEW_TEXT", "DETAIL_TEXT");
        $arFilter = Array("IBLOCK_ID"=> IntVal($iblockId), "ID" => $elementIds);
        $blockResult = \CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);


        while($ob = $blockResult->GetNext())
        {
            $el = new \CIBlockElement;
            \Dbe\DynamicEditorParser::parse($ob['~PREVIEW_TEXT']);
            \Dbe\DynamicEditorParser::parse($ob['~DETAIL_TEXT']);
            $arLoadProductArray = Array(
                "PREVIEW_TEXT" => $ob['~PREVIEW_TEXT'],
                "DETAIL_TEXT" => $ob['~DETAIL_TEXT'],
            );
            $el->Update($ob['ID'], $arLoadProductArray);
        }
    }
    private static function getIblockContentPropsConvertAndSave($iblockId, $elementIds, $propIds)
    {
        $result = array();
        $resultMultiple = array();
        foreach ($elementIds as $elementId) {
            foreach ($propIds[$iblockId][$elementId] as $propId) {
                $res = \CIBlockElement::GetProperty($iblockId, $elementId, "sort", "asc", array('ID' => $propId));
                while ($ob = $res->GetNext()) {
                    if ($ob['MULTIPLE'] == 'Y') {
                        $resultMultiple[$elementId][$ob['ID']][$ob['PROPERTY_VALUE_ID']] = $ob['~VALUE']['TEXT'];
                    } else {
                        $result[$elementId][$ob['ID']] = $ob['~VALUE']['TEXT'];
                    }
                }
            }
        }

        self::convertPropsAndSave($iblockId, $result, $resultMultiple);
    }

    private static function convertPropsAndSave($iblockId, $result, $resultMultiple)
    {

        foreach ($result as $elementId => $items) {
            foreach ($items as $id => $value) {
                \Dbe\DynamicEditorParser::parse($value);
                \CIBlockElement::SetPropertyValuesEx($elementId, $iblockId, array($id => array('VALUE' =>  array('TYPE' => 'HTML', 'TEXT' => $value))));
            }
        }

        foreach ($resultMultiple as $elementId => $items) {
            foreach ($items as $id => $prop) {
                $saveProps = array();
                foreach ($prop as $propId => $value) {
                    \Dbe\DynamicEditorParser::parse($value);
                    $saveProps[] = array('VALUE' => array('TYPE' => 'HTML', 'TEXT' => $value));
                }
                \CIBlockElement::SetPropertyValuesEx($elementId, $iblockId, array($id => $saveProps));
            }
        }
    }
}
