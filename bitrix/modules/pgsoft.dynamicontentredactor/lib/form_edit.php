<?php

namespace  Dbe;
\CModule::IncludeModule("iblock");

class DbeFormEdit{
    public static function getIblocks($iblockId = null){
        $arRes = array();
        $res = \CIBlock::GetList(
            Array(),
            Array(), true
        );
        while($ar_res = $res->Fetch())
        {
            $arRes[$ar_res['ID']] = array('ACTIVE' => ($iblockId == $ar_res['ID'] ? 'Y' : ''),'VAL' => $ar_res['NAME'].'('.$ar_res['ID'].')', 'TYPE' => $ar_res['IBLOCK_TYPE_ID']);
        }
        return $arRes;
    }
    public static function mapArray($arTypesEx,$active){
        $arTypes = array();
        foreach($arTypesEx as $key=>$val){
            if(is_array($active) && in_array($key,$active)){
                $arTypes[$key]['ACTIVE'] = 'Y';
            } else if($active == $key){
                $arTypes[$key]['ACTIVE'] = 'Y';
            }

            $arTypes[$key]['VAL'] = $val;
        }
        return $arTypes;
    }
    public static function getIblockTypes($type = null){
        $arTypesEx = self::mapArray(\CIBlockParameters::GetIBlockTypes(),$type);
        return $arTypesEx;
    }
    public static function getIblockFields($arActive = null){
        $fields = \CIBlockParameters::GetFieldCode(GetMessage("IBLOCK_FIELD"), "DATA_SOURCE");
        return  self::mapArray($fields['VALUES'],$arActive);
    }
    public static function getIblockProps($iblockId,$arActive)
    {
        if ($iblockId) {
            $rsProp = \CIBlockProperty::GetList(array("sort" => "asc", "name" => "asc"), array("ACTIVE" => "Y", "IBLOCK_ID" => $iblockId));
            while ($arr = $rsProp->Fetch()) {

                if(is_array($arActive) && in_array($arr['CODE'],$arActive)){
                    $arProperty[$arr['CODE']]['ACTIVE'] = 'Y';
                } else if($arActive == $arr['CODE']){
                    $arProperty[$arr['CODE']]['ACTIVE'] = 'Y';
                }
                if($arr['MULTIPLE'] == 'Y'){
                    $arProperty[$arr["CODE"]]['MULTIPLE'] = 'Y';
                }
                $arProperty[$arr["CODE"]]['VAL'] = "[" . $arr["CODE"] . "] " . $arr["NAME"];
            }
            return $arProperty;
        }
    }
    public static function addDbe($params){
        \CModule::includeModule('pgsoft.dynamicontentredactor');
        $result = \Dbe\Tables\DbeTable::add($params);
        if (!$result->isSuccess())
        {
            $errors = $result->getErrorMessages();
        }
    }
    public static function updateDbe($id,$params){
        \CModule::includeModule('pgsoft.dynamicontentredactor');
        $result = \Dbe\Tables\DbeTable::update($id,$params);
        if (!$result->isSuccess())
        {
            $errors = $result->getErrorMessages();
        }
    }
    public static function getDbeFields($id){
        \CModule::includeModule('pgsoft.dynamicontentredactor');
        $arFields = \Dbe\Tables\DbeTable::getList(
            array(
                'select' => array('*'),
                'filter' => array('id' => $id)
            )
        )->fetchAll();
        return $arFields[0];
    }
    public static function saveFileById($iName){
        if($_FILES[$iName]['name']){
            $arr_file=Array(
                "name" => $_FILES[$iName]['name'],
                "size" => $_FILES[$iName]['size'],
                "tmp_name" => $_FILES[$iName]['tmp_name'],
                "type" => "",
                "old_file" => "",
                "del" => "Y",
                "MODULE_ID" => "iblock"
            );
            $fid = \CFile::SaveFile($arr_file, "dynamicblockeditors");
            return $fid;
        }
    }
    public static function execEdit(){
        $arTitles = array();
        $errors = array();
        if($_REQUEST['action'] == 'add'){
            $action = 'add';
            $arTitles['TAB_NAME'] = 'Добавить снипет';
        }
        if($_REQUEST['action'] == 'delete'){
            $action = 'delete';
            $id = $_REQUEST['id'];
        }
        if($_REQUEST['action'] == 'edit' && $_REQUEST['id']){
            $action = 'edit';
            $id = $_REQUEST['id'];
            $arTitles['TAB_NAME'] = 'Обновить снипет';
        }
        if(!$action){
            return false;
        }

        if($_POST['save'] && $action == 'edit'){
            $iblockFields  = array();
            if($_REQUEST['iblock_fields']){
                $iblockFields['fields'] = $_REQUEST['iblock_fields'];
            }
            if($_REQUEST['iblock_properties']){
                $iblockFields['properties'] = $_REQUEST['iblock_properties'];
            }

            $iconId = self::saveFileById('icon');
            $updateFields = array(
                'name' => $_REQUEST['name'],
                'iblock_id' => $_REQUEST['iblock_id'],
                'iblock_fields' => $iblockFields['fields'],
                'iblock_properties' => $iblockFields['properties'],
                'template' => $_REQUEST['template']
            );
            if($iconId){
                $updateFields['icon'] = $iconId;
            }

            if(
                empty($_FILES['icon']) ||
                empty($_REQUEST['name']) ||
                //empty($_REQUEST['iblock_id']) ||
                empty($_REQUEST['template'])
            ){
                $errors[] = 'Не все обязательные поля заполнены';

            }else {
                \Dbe\DbeFormEdit::updateDbe($id,$updateFields);
            }
        }
        if((!$_POST['save']) && $action == 'delete'){
            \Dbe\Tables\DbeTable::delete($id);
        }
        if($action == 'edit'){
            $arFields = \Dbe\DbeFormEdit::getDbeFields($id);

            $iblocks = \Dbe\DbeFormEdit::getIblocks($arFields['iblock_id']);
            foreach($iblocks as $val){
                if($val['ACTIVE']){
                    $iblockType = $val['TYPE'];
                    break;
                }
            }
            $iblockTypes = \Dbe\DbeFormEdit::getIblockTypes($iblockType);
           // print_r($arFields);
            $iblockProperties = \Dbe\DbeFormEdit::getIblockProps($arFields['iblock_id'],$arFields['iblock_properties']);
            $iblockFields = \Dbe\DbeFormEdit::getIblockFields($arFields['iblock_fields']);
        }else{
            $iblockTypes = \Dbe\DbeFormEdit::getIblockTypes();
            $iblocks = \Dbe\DbeFormEdit::getIblocks();
            $iblockFields = \Dbe\DbeFormEdit::getIblockFields();
        }

        if ($_POST['save'] && check_bitrix_sessid() && $action == 'add')
        {

            $iblockFields  = array();
            if($_REQUEST['iblock_fields']){
                $iblockFields['fields'] = $_REQUEST['iblock_fields'];
            }
            if($_REQUEST['iblock_properties']){
                $iblockFields['properties'] = $_REQUEST['iblock_properties'];
            }



            $addFields = array(
                'name' => $_REQUEST['name'],
                'iblock_id' => $_REQUEST['iblock_id'],
                'iblock_fields' => $iblockFields['fields'],
                'iblock_properties' => $iblockFields['properties'],
                'template' => $_REQUEST['template']
            )  ;
            $arFields = $addFields;

            $iblocks = \Dbe\DbeFormEdit::getIblocks($arFields['iblock_id']);
            foreach($iblocks as $val){
                if($val['ACTIVE']){
                    $iblockType = $val['TYPE'];
                    break;
                }
            }

            $iblockTypes = \Dbe\DbeFormEdit::getIblockTypes($iblockType);
            // print_r($arFields);
            $iblockProperties = \Dbe\DbeFormEdit::getIblockProps($arFields['iblock_id'],$arFields['iblock_properties']);
            $iblockFields = \Dbe\DbeFormEdit::getIblockFields($arFields['iblock_fields']);

            //print_r($iblockProperties);
            $iconId = self::saveFileById('icon');
            if($iconId){
                $addFields['icon'] = $iconId;
            }
            if(
                empty($addFields['icon']) ||
                empty($addFields['name']) ||
                //empty($addFields['iblock_id']) ||
                empty($addFields['template'])
            ){
                $errors[] = 'Не все обязательные поля заполнены';




            }else {
                \Dbe\DbeFormEdit::addDbe($addFields);
                global $APPLICATION;
                LocalRedirect($APPLICATION->GetCurPage());
            }

        }
        //print_r($errors);
        return array(
            'TITLES' => $arTitles,
            'IBLOCK_TYPE' => $iblockType,
            'FORM_FIELDS' => $arFields,
            'IBLOCK_PROPERTIES' => $iblockProperties,
            'IBLOCK_FIELDS' => $iblockFields,
            'IBLOCK_TYPES' => $iblockTypes,
            'IBLOCKS' => $iblocks,
            'ERRORS' => $errors

        );


    }
}