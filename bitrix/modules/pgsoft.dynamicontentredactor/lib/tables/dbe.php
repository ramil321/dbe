<?php
namespace Dbe\Tables;

use Bitrix\Main\Entity;
use Bitrix\Main\Type;

class DbeTable extends Entity\DataManager
{
    public static function getFilePath()
    {
        return __FILE__;
    }

    public static function getTableName()
    {
        return 'dynamicontentredactor';
    }

    public static function getMap()
    {
        return array(
            new Entity\IntegerField('id', array(
                    'primary' => true,
                    'autocomplete' => true,
                )
            ),
            new Entity\BooleanField('active', array(
                'values' => array('N', 'Y'),
                'default_value' => 'Y',
            )),
            new Entity\StringField('name', array(
                    'required' => true,
                    'validation' => function(){
                        return array(
                            new Entity\Validator\Length(null, 20),
                        );
                    }
                )
            ),
            new Entity\IntegerField('iblock_id', array(
                    'validation' => function(){
                        return array(
                            new Entity\Validator\Length(null, 20),
                        );
                    }
                )
            ),
            new Entity\IntegerField('icon', array(
                    'validation' => function(){
                        return array(
                            new Entity\Validator\Length(null, 20),
                        );
                    }
                )
            ),
            new Entity\TextField('iblock_fields', array(

                    'validation' => function(){
                        return array(
                            new Entity\Validator\Length(null, 1000),
                        );
                    },
                    'save_data_modification' => function () {
                        return array(
                            function ($value) {
                                return serialize($value);
                            }
                        );
                    },
                    'fetch_data_modification' => function () {
                        return array(
                            function ($value) {
                                return unserialize($value);
                            }
                        );
                    }
                )
            ),
            new Entity\TextField('iblock_properties', array(

                    'validation' => function(){
                        return array(
                            new Entity\Validator\Length(null, 1000),
                        );
                    },
                    'save_data_modification' => function () {
                        return array(
                            function ($value) {
                                return serialize($value);
                            }
                        );
                    },
                    'fetch_data_modification' => function () {
                        return array(
                            function ($value) {
                                return unserialize($value);
                            }
                        );
                    }
                )
            ),
            new Entity\TextField('template', array(
                    'required' => true
                )
            ),
            new Entity\DatetimeField('created_at', array(
                    'required' => true,
                    'default_value' => new Type\DateTime,
                    'validation' => function(){
                        return array(
                            new Entity\Validator\Date(),
                        );
                    }
                )
            ),
            new Entity\DatetimeField('status_at', array(
                    'required' => true,
                    'default_value' => new Type\DateTime,
                    'validation' => function(){
                        return array(
                            new Entity\Validator\Date(),
                        );
                    }
                )
            )
        );
    }

}