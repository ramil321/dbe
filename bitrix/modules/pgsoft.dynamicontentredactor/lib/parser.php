<?php

namespace  Dbe;
\CModule::IncludeModule('iblock');

class DynamicEditorParser{
    public static $arDbe = array();
    public static $prefix = 'block_dynamic_editor';
    public static function getDbeArray(){
        if(self::$arDbe){
            return self::$arDbe;
        }
        \CModule::includeModule('pgsoft.dynamicontentredactor');
        $dbeList = self::getDbeList();
        foreach($dbeList as $val){
            $dbe = array();
            $dbe['id'] = $val['id'];
            $dbe['object'] = new \Dbe\DynamicEditorBlock(
                self::$prefix.$val['id'],
                $val['template'],
                $val['iblock_id'],
                $val['iblock_fields'],
                $val['iblock_properties']
            );
            $file = \CFile::ResizeImageGet($val['icon'], array('width'=>150, 'height'=>16), BX_RESIZE_IMAGE_PROPORTIONAL, true);
            $fileEditor = \CFile::ResizeImageGet($val['icon'], array('width'=>128, 'height'=>128), BX_RESIZE_IMAGE_PROPORTIONAL, true);
            $fileEditorNew = \CFile::ResizeImageGet($val['icon'], array('width'=>20, 'height'=>20), BX_RESIZE_IMAGE_PROPORTIONAL, true);
            $hideSelectIblockWindow = '';
            if(!$val['iblock_id']){
                $hideSelectIblockWindow = 1;
            }
            $dbe['script_default'] = 'DynamicBlockReg.iblockElementId ='.$_REQUEST['ID'];
            $dbe['script'] = 'DynamicBlockReg.addBlock("'.self::$prefix.$val['id'].'",'.$val['iblock_id'].',"'.self::getIblockCode($val['iblock_id']).'", function(obj,content){ obj.pMainObj.PasteHtmlAtCaret(content); }, "'.$file['src'].'", "'.$hideSelectIblockWindow.'");';
            $dbe['script_new'] = 'DynamicBlockReg.addBlock("'.self::$prefix.$val['id'].'",'.$val['iblock_id'].',"'.self::getIblockCode($val['iblock_id']).'", function(obj,content){ obj.InsertHtml(content); }, "'.$file['src'].'", "'.$hideSelectIblockWindow.'");';
            $dbe['script_button'] = 'DynamicBlockRedactorButtonsReg.add("'.self::$prefix.$val['id'].'","'.self::$prefix.$val['id'].'","'.$val['name'].'", "'.$fileEditor['src'].'", "'.$fileEditor['src'].'", "'.$hideSelectIblockWindow.'");';
            $dbe['script_button_new'] = 'DynamicBlockRedactorButtonsReg.addNew("'.self::$prefix.$val['id'].'","'.self::$prefix.$val['id'].'","'.$val['name'].'", "'.$fileEditorNew['src'].'", "'.$fileEditorNew['src'].'", "'.$hideSelectIblockWindow.'");';

            self::$arDbe[self::$prefix.$val['id']] = $dbe;
        }
        return self::$arDbe;
    }
    function checkArrayUnique($array) {
        return sizeof($array) !== sizeof(array_unique($array));
    }
    public static function parse(&$content, $listId = null){
        $arParse = array();
        $dbeArray = self::getDbeArray();
        $parseTrigger = true;
        $lvl = 0;
        while($parseTrigger){
            $checkParse = false;
            foreach($dbeArray as $val){

                if(isset($listId) && $val['id'] != $listId) {
                    continue;
                }

                if($val['object']->OnEndBufferContent($content)){
                    $checkParse = true;
                }
            }
            $lvl++;
            if( ($lvl > 10) || ($checkParse == false) ) {
                $parseTrigger = false;
            }
        }
    }
    public static function addScriptsRedactor(){
        $dbeArray = self::getDbeArray();
        global $APPLICATION;
        foreach($dbeArray as $val){
            $APPLICATION->AddHeadString('<script>'.$val['script_default'].'</script>',true);
            $APPLICATION->AddHeadString('<script>'.$val['script'].'</script>',true);
            $APPLICATION->AddHeadString('<script>'.$val['script_button'].'</script>',true);
        }
    }
    public static function addScriptsRedactorNew(){
        $dbeArray = self::getDbeArray();
        global $APPLICATION;
        foreach($dbeArray as $val){
            $APPLICATION->AddHeadString('<script>'.$val['script_default'].'</script>',true);
            $APPLICATION->AddHeadString('<script>'.$val['script_new'].'</script>',true);
            $APPLICATION->AddHeadString('<script>'.$val['script_button_new'].'</script>',true);
        }
    }
    public static function getIblockCode($id){
        if($id) {
            \CModule::IncludeModule('iblock');
            $res = \CIBlock::GetList(
                Array(),
                Array('ID' => $id), true
            );
            if ($ar_res = $res->Fetch()) {
                return $ar_res['IBLOCK_TYPE_ID'];
            }
        }
    }
    public static function getDbeList(){
        \CModule::includeModule('pgsoft.dynamicontentredactor');
        $arFields = \Dbe\Tables\DbeTable::getList(
            array(
                'select' => array('*')
            )
        )->fetchAll();
        return $arFields;
    }
}
