<?php

namespace  Dbe;
\CModule::IncludeModule('iblock');

class DynamicEditorBlock{
    public $id;
    private $pattern = '';
    private $patternEmpty  = '';
    private $iblockId = '';
    private $fields;
    private $property;
    private $empty = '';
    private $tplPath = '';
    private $elements = array();
    private $arParsed = array();
    private $tempGetElRes = array();
    private $tmpContent = array();
    public function __construct($id,$tplPath,$iblockId,$fields,$property) {
        $this->id = $id;
        $this->pattern = $this->getPatten($id);
        $this->patternEmpty = '/<img[^>]*class="'.$id.' dbe_id_empty"(.*?)>/';
        $this->iblockId = $iblockId;
        $this->fields = $fields;
        $this->property = $property;
        $this->tplPath = $tplPath;
    }
    public function getPatten($id = null, $findId = null){
        if($findId){
            $reg = $findId;
        }else {
            $reg = '(\d+?)';
        }
        return '/<img[^>]*class="'.$id.' dbe_id_'.$reg.'"(.*?)>/';
    }
    public function OnEndBufferContent(&$content){
        $this->arParsed = array();
        $ids = $this->findIds($content);
        if($this->iblockId && $ids) {
            $arElemets = $this->getElementsById($ids);
            foreach ($arElemets as &$item) {
                ob_start();
                $this->showTemplate($item);
                $item['content'] = ob_get_clean();
            }
            $this->elements = $arElemets;
        }else {
            $empty = $this->findEmpty($content);
            if($empty){
                ob_start();
                $this->showTemplate();
                $this->empty = ob_get_clean();
            }
        }
        if($this->elements){
            $string = $content;
            $content = preg_replace_callback($this->pattern, function($mathes){
                $this->arParsed[] = $mathes[1];
                if($curContent = $this->elements[$mathes[1]]['content']){
                   $curContent = preg_replace($this->getPatten($this->getId(),$mathes[1]), '', $curContent);
                }
                return $curContent;
            }, $string);
        }
        if($this->empty){
            $string = $content;
            $content = preg_replace_callback($this->patternEmpty, function($mathes){
                $this->arParsed[] = 'empty';
                return $this->empty;
            }, $string);
        }
        return $this->arParsed;
    }
    public function findIds($content){
        preg_match_all($this->pattern, $content, $matches);
        return array_unique($matches[1]);
    }
    public function findEmpty($content){
        preg_match_all($this->patternEmpty, $content, $matches);
        return array_unique($matches[0]);
    }
    public static function getEl($iblockId = null, $fields = null, $props = null, $ids = null){
        if(!$ids){
            return array();
        }

        $arSelect = array_merge($fields, array(
            "ID",
            "IBLOCK_ID",
        ));

        $arFilter = array (
            "IBLOCK_ID" => $iblockId,
            "ACTIVE" => "Y",
            "CHECK_PERMISSIONS" => "N",
        );

        $arFilter['ID'] = $ids;
        $rsElement = \CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
        while($obElement = $rsElement->GetNextElement())
        {
            $arItem = $obElement->GetFields();
            $arProps = $obElement->GetProperties();
            foreach($arProps as $val){
                if(in_array($val['CODE'],$props)){
                    $arItem['PROPERTY_'.$val['CODE']] = $val['VALUE'];
                    $arItem['~PROPERTY_'.$val['CODE']] = $val['~VALUE'];
                }
            }

            $arResult[] = $arItem;
        }
        return $arResult;
    }
    public function showTemplate($arResult){
        eval("?> ".$this->tplPath." <?php ");
    }
    public function getElementsById($ids){
        if($this->tempGetElRes[implode($ids)]) {
            return $this->tempGetElRes[implode($ids)];
        }
        $res = $this->getEl($this->iblockId,$this->fields,$this->property,$ids);
        $arResult = array();
        foreach ($res as $val){
            $arResult[$val['ID']] = $val;
        }
        $this->tempGetElRes[implode($ids)] = $arResult;
        return $arResult;

    }
    public function getId(){
        return $this->id;
    }
}
