var DbeEditForm = {
    editor : '',
    editorInit : function(){
        DbeEditForm.editor = ace.edit("dbe_edite_template");
        DbeEditForm.editor.setTheme("ace/theme/chrome");
        DbeEditForm.editor.session.setMode("ace/mode/php");
        var textarea = $('textarea[name="template"]').hide();
        DbeEditForm.editor.getSession().setValue(textarea.val());
        DbeEditForm.editor.getSession().on('change', function(){
            textarea.val(DbeEditForm.editor.getSession().getValue());
        });
    },
    generateParamList : function(){
        $('#dbe_edit_available_fields').html('');
        var dbeaf = '';
        $('#dbe_edit_iblock_fields :selected').each(function () {
            dbeaf += '<a class="dbe_edit_helper_link" data-content="'+$(this).val()+'" href="#'+$(this).val()+'">'+$(this).val()+'<a> ';
        });
        $('#dbe_edit_iblock_props :selected').each(function () {
            // console.log($(this));
            //  alert($(this).find('option').attr('data-multiple'));
            dbeaf += '<a class="dbe_edit_helper_link" data-multiple="'+$(this).attr('data-multiple')+'" data-content="PROPERTY_'+$(this).val()+'" href="#'+$(this).val()+'">PROPERTY_'+$(this).val()+'<a> ';
        });
        $('#dbe_edit_available_fields').html(dbeaf);
    },
    getIblockProps : function (iblockId) {
        $.getJSON( "/bitrix/tools/dynamicontentredactor.php?action=getprop&iblock_id="+iblockId, function(data) {
            $('#dbe_edit_iblock_props').html('');
            if(data){
                $.each(data,function (index,val) {
                    $('#dbe_edit_iblock_props').append('<option value="'+index+'">'+val.VAL+'</option>');
                });
            }
        })
    },
    addHelpLink : function (cur) {
        var txtToAdd = $(cur).data('content');
        if($(cur).attr('data-multiple') == 'Y'){
            var list =  '\n<ul>\n' +
                '    <?foreach($arResult["PROPERTY_MORE_'+txtToAdd+'"] as $val):?>\n' +
                '        <li><?=$val?></li>\n' +
                '    <?endforeach?>\n' +
                '</ul>\n';
            txtToAdd  = list;
        }else{
            txtToAdd  = '<?=$arResult["'+txtToAdd+'"]?>';
        }


        DbeEditForm.editor.session.insert(DbeEditForm.editor.getCursorPosition(),txtToAdd );
    },
    init : function(){
        $('#dbe_edit_iblock_type').change(function () {
            $('#dbe_edit_iblock_id').val('');
            $('#dbe_edit_iblock_id option').hide();
            $('#dbe_edit_iblock_id .dbe_edit_iblock_id_'+$(this).val()).show();
            $('#dbe_edit_iblock_id_default').show();
        });
        $('body').on('click','.dbe_edit_helper_link',function (e) {
            e.preventDefault();
            DbeEditForm.addHelpLink(this);
        });
        $('#dbe_edit_iblock_id').change(function () {
            if($(this).val()){
                DbeEditForm.getIblockProps($(this).val());
            }
        });

        $('body').on('change','#dbe_edit_iblock_props',function () {
            DbeEditForm.generateParamList();
        });
        $('body').on('change','#dbe_edit_iblock_fields',function () {
            DbeEditForm.generateParamList();
        });

        $('#dbe_edit_iblock_props').change(function(){ DbeEditForm.generateParamList();});
        $('#dbe_edit_iblock_fields').change(function(){ DbeEditForm.generateParamList();});

        $('#dbe_edit_field_code_add').click(function () {
            $('.dbe_edit_field_code').last().after('<br/>'+$('.dbe_edit_field_code').last()[0].outerHTML);
        });
        DbeEditForm.editorInit();
        DbeEditForm.generateParamList();
    }
}