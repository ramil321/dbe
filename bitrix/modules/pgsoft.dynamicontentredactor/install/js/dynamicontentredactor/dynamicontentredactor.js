function DynamicBlockEditorVar(id,iblockId,typeIblock,addContentFunction,icon)
{
    var that = this;
    this.event1 = '';
    this.event2 = '';
    this.style = '';
    this.curObj = '';
    this.icon = '';
    this.replaceAddValMethod = function()
    {
        JCMainLookupAdminSelector.prototype.AddValue = function(arValue)
        {

            if (typeof arValue != 'object' || null == arValue.length || null == arValue[0])
                arValue = [arValue];

            var _this = this;
            for (var i = 0; i < arValue.length; i++)
            {
                if (typeof arValue[i] == 'object')
                {
                    if (null != arValue[i].ID && null != arValue[i].NAME)
                    {
                        this.VISUAL.AddTokenData(arValue[i], false);
                    }
                }
                else
                {
                    var val = parseInt(arValue[i]);

                    if (!isNaN(val))
                    {
                        if(that.event2){
                            that.event2(val);
                        }
                        var str = 'q <q@q> [' + val + ']'; // hack
                        var url = this.arParams.AJAX_PAGE+'?MODE=SEARCH';

                        url += '&search=' + encodeURIComponent(str);
                        if (this.arParams.AJAX_PARAMS)
                        {
                            for(var param_name in this.arParams.AJAX_PARAMS)
                                url += '&' + param_name + '=' + encodeURIComponent(this.arParams.AJAX_PARAMS[param_name]);
                        }
                        BX.ajax.get(url, function(data) {
                            if (data.length <= 0)
                                return;


                            var DATA = [];
                            eval('DATA = ' + data);
                            if (DATA.length == 1 && DATA[0].READY == 'Y')
                            {

                                that.setBlock(DATA[0].ID,DATA[0].NAME,this.curObj);
                                _this.VISUAL.AddTokenData(DATA[0], false);
                            }
                        });
                    }
                }
            }
        };
    },
        this.init = function(id,iblockId,typeIblock,addContentFunction,icon)
        {
            // alert('init');
            this.id = id;
            this.name = id+'name';
            this.layout = id+'layout';
            this.auto = id+'auto';
            this.typeIblock = typeIblock;
            this.iblockId = iblockId;
            this.addContentFunction = addContentFunction;
            this.icon = icon;
        },
        this.showElementWindowAdd = function(curObj)
        {
            this.curObj = curObj;
            this.selectorInit();
            jsUtils.OpenWindow('/bitrix/admin/iblock_element_edit.php?IBLOCK_ID='+this.iblockId+'&type='+this.typeIblock+'&lang=ru&IBLOCK_SECTION_ID=-1&find_section_section=-1&lookup='+this.id, 900, 600);

        },
        this.setBlock = function(id,name,curObj)
        {
            var curCursor = this.curObj;
            if(curObj){
                curCursor = curObj;
            }
            console.log(curCursor);
            if(curCursor && id){
                this.saveDbeContent(curCursor, this.iblockId, DynamicBlockReg.iblockElementId, this.id);
                var html = '<img title="'+name+'" class="'+this.id+' dbe_id_'+id+'" src="'+this.icon+'" />';
                this.addContentFunction(curCursor,html);
                this.curObj = '';
            }
        } ,
        this.showElementWindowEdit = function(id)
        {
            this.selectorInit();
            jsUtils.OpenWindow('/bitrix/admin/iblock_element_edit.php?ID='+id+'&IBLOCK_ID='+this.iblockId+'&type='+this.typeIblock+'&lang=ru&IBLOCK_SECTION_ID=-1&find_section_section=-1&lookup='+this.id, 900, 600);
        },
        this.selectorInit = function()
        {
            $('body').append('<div style="display: none" id="'+this.layout+'"><input type="hidden" name="'+this.name+'" value=""><input autocomplete="off" type="text" name="myvisual" id="'+this.auto+'" value="" class="mli-field" style="width: 95%;"></div>');

            this.replaceAddValMethod();
            window[this.id] = new JCMainLookupAdminSelector({
                'AJAX_PAGE':'/bitrix/components/bitrix/main.lookup.input/templates/iblockedit/ajax.php',
                'AJAX_PARAMS':{'IBLOCK_ID':this.iblockId,'lang':'ru','site':'ru',
                    'admin':'Y','TYPE':'ELEMENT','BAN_SYM':',;','REP_SYM':' '},
                'CONTROL_ID':this.id,
                'LAYOUT_ID':this.layout,
                'INPUT_NAME':this.name,
                'PROACTIVE':'MESSAGE','VALUE':[],
                'VISUAL':{'ID':this.auto,'MAX_HEIGHT':'1000','MIN_HEIGHT':'30','START_TEXT':'введите текст','SEARCH_POSITION':'absolute','SEARCH_ZINDEX':'4000'
                }});

        },
        this.showElementWindowSelect = function(curObj){
            this.curObj = curObj;
            this.selectorInit();

            if(curObj){
                jsUtils.OpenWindow('/bitrix/admin/iblock_element_search.php?lang=ru&IBLOCK_ID='+this.iblockId+'&n=&k=&lookup='+this.id, 900, 600);
            }
        },
        this.loadFrameFunc = function()
        {

        },
        this.saveDbeContent = function(curCursor, iblockId, iblockElementId, dbeId)
        {
            var sendObj = {
                'iblock_id' : iblockId,
                'iblock_element_id' : iblockElementId,
                'iblock_fields' : '',
                'iblock_prop' : '',
                'dynamicontentredactor_id' : dbeId
            };
            var inputNameParts = curCursor.config.inputName.split('_');
            if (inputNameParts[0] == 'PROP') {
                sendObj.iblock_prop = inputNameParts[1];
            } else {
                sendObj.iblock_fields = curCursor.config.inputName;
            }

            $.ajax({
                type: "POST",
                url: '/bitrix/tools/dynamicontentredactor.php?action=setcontent',
                data: sendObj,
            });
        }
    this.init(id,iblockId,typeIblock,addContentFunction,icon);
}


var DynamicBlockReg =
    {
        iblockElementId : '',
        blocks : [],
        loaded : false,
        addBlock : function(id,iblockId,typeIblock,addContentFunction,icon,hideSelectIblockWindow){
            var dy = new DynamicBlockEditorVar(id,iblockId,typeIblock,addContentFunction,icon);
            this.blocks.push({'name': id, 'val': dy, 'hideSelectIblockWindow' : hideSelectIblockWindow});
        },
        frameInit : function()
        {
            console.log('frameinit');
            if(!DynamicBlockReg.loaded){
            }
        },
        loadFrameFunc : function()
        {
            $(function(){
                if(DynamicBlockReg.blocks.length){
                    DynamicBlockReg.blocks.forEach(function(item, i, arr){
                        $(".bx-editor-iframe").contents().find('body').on('click','.'+item.val.id ,function( event ) {
                            var elementId = '';
                            var classList = $(this).attr('class').split(/\s+/);
                            $.each(classList, function(i, className) {
                                if (className !== item.val.id) {
                                    elementId = className.replace("dbe_id_", "");
                                }
                            });
                            if(!item.hideSelectIblockWindow) {
                                item.val.showElementWindowEdit(elementId);
                            }
                        });
                    });
                }
            });
        },
        getBlock : function(id)
        {
            var el = false;
            if(this.blocks.length){
                this.blocks.forEach(function(item, i, arr) {
                    if(id == item.name){
                        el = item.val;
                        return;
                    }
                });
            }
            return el;
        }
    }
var DynamicBlockDialog =
    {
        show : function(redactor,blockName,name)
        {
            var Dialog = new BX.CDialog({
                title: name,
                icon: 'head-block',
                resizable: false,
                draggable: true,
                height: '0',
                width: '0',
                buttons: [BX.CDialog.btnClose,
                    {
                        title: 'Добавить',
                        name: 'Добавить',
                        id: 'youtube',
                        action: function () {
                            var dy = DynamicBlockReg.getBlock(blockName);
                            if(dy){
                                dy.showElementWindowAdd(redactor);
                            }
                            Dialog.Hide();
                        }

                    },
                    {
                        title: 'Выбрать',
                        name: 'Выбрать',
                        id: 'youtube',
                        action: function () {
                            var dy = DynamicBlockReg.getBlock(blockName);
                            if(dy){
                                dy.showElementWindowSelect(redactor);
                            }
                            Dialog.Hide();
                        }

                    }
                ]
            });
            Dialog.Show();
        }
    }
var DynamicBlockRedactorButtonsReg =
    {
        blocks : [],
        redactorContentStyle : '',
        newRedactorObj : '',
        add : function (id,dbeId,name,icon,iconContent,hideSelectIblockWindow)
        {
            var block = [ 'BXButton', {
                id : id,
                src : icon,
                name : name,
                title : name,
                show_name : false,
                handler: function(){
                    curObj = this;
                    if(hideSelectIblockWindow){
                        var dy = DynamicBlockReg.getBlock(dbeId);
                        if(dy){
                            dy.setBlock(id,name,curObj);
                        }
                    }else{
                        DynamicBlockDialog.show(curObj,dbeId,name);
                    }
                }
            }];
            this.blocks.push({'name': id, 'event': block});
            this.addStyle(dbeId,iconContent);

        },
        addNew : function (id,dbeId,name,icon,iconContent,hideSelectIblockWindow)
        {
            var block = {
                iconClassName: 'bxhtmled-button-sotbit-add-img',
                /*Путь до иконки кнопки*/
                src: icon,
                id: id,
                /*Названия кнопки из языкового файла*/
                name: name,
                /*Событие по нажатию на кнопку*/
                handler:function(e){
                    console.log()
                    if(hideSelectIblockWindow){
                        var dy = DynamicBlockReg.getBlock(dbeId);
                        if(dy){
                            dy.setBlock('empty',name,this.editor);
                        }
                    }else {
                        DynamicBlockDialog.show(this.editor, dbeId, name);
                    }
                }
            }
            this.blocks.push({'name': id, 'event': block});
            this.addStyle(dbeId,iconContent);
        },
        getList : function ()
        {
            return this.blocks;
        },
        getStyle : function ()
        {
            return this.redactorContentStyle;
        },
        addStyle : function (dbeId,icon)
        {
            var style = '.'+dbeId+' {' +
                'padding-left: 7px;' +
                'background-image: url('+icon+');' +
                'padding-right: 7px;' +
                'background-size: contain;' +
                'background-repeat: no-repeat;' +
                '}  ';
            this.redactorContentStyle = this.redactorContentStyle + style;
        }
    }