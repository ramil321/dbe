<?

use Bitrix\Main\Localization\Loc;

IncludeModuleLangFile(__FILE__);
Class pgsoft_dynamicontentredactor extends CModule
{
	const MODULE_ID = 'pgsoft.dynamicontentredactor';
	var $MODULE_ID = 'pgsoft.dynamicontentredactor';
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $strError = '';
	private $type = 'mysql';
	function __construct()
	{
		$arModuleVersion = array();
		include(dirname( __FILE__ ) . "/version.php");
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage("pgsoft.dynamicontentredactor_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("pgsoft.dynamicontentredactor_MODULE_DESC");

		$this->PARTNER_NAME = GetMessage("pgsoft.dynamicontentredactor_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("pgsoft.dynamicontentredactor_PARTNER_URI");
	}
	function addEditorScript(){
		//$script = "<script>console.log('test')</script>";
		$script = file_get_contents(dirname(__FILE__).'js/admin/htmleditor2/dynamicontentredactor_editor.js');
		file_put_contents($_SERVER['DOCUMENT_ROOT']."/bitrix/admin/htmleditor2/dynamicontentredactor_editor.js", $script);
		$scriptNew = file_get_contents(dirname(__FILE__).'js/admin/htmleditor2/dynamicontentredactor_editor_new.js');
		file_put_contents($_SERVER['DOCUMENT_ROOT']."/bitrix/admin/htmleditor2/dynamicontentredactor_editor_new.js", $scriptNew);

	}
	function InstallDB($arParams = array())
	{
		// $this->addEditorScript();
		RegisterModuleDependences('main', 'OnBuildGlobalMenu', self::MODULE_ID, 'Dbe\CPgsoftDynamicontentredactor', 'OnBuildGlobalMenu');
		RegisterModuleDependences('fileman', 'OnIncludeHTMLEditorScript', self::MODULE_ID, 'Dbe\CPgsoftDynamicontentredactor', 'onIncludeHTMLEditorHandler');
		RegisterModuleDependences('fileman', 'OnBeforeHTMLEditorScriptsGet', self::MODULE_ID, 'Dbe\CPgsoftDynamicontentredactor', 'onBeforeHTMLEditorScriptsGet');
		RegisterModuleDependences('fileman', 'OnBeforeHTMLEditorScriptRuns', self::MODULE_ID, 'Dbe\CPgsoftDynamicontentredactor', 'onBeforeHTMLEditorScriptRuns');
		RegisterModuleDependences('main', 'OnEndBufferContent', self::MODULE_ID, 'Dbe\CPgsoftDynamicontentredactor', 'onEndBufferContent');
		RegisterModuleDependences('search', 'BeforeIndex', self::MODULE_ID, 'Dbe\CPgsoftDynamicontentredactor', 'onBeforeIndex');

		global $DB, $DBType, $APPLICATION;
		$this->errors = false;
		$this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".self::MODULE_ID."/install/db/".strtolower($DB->type)."/install.sql");


		return true;
	}

	function UnInstallDB($arParams = array())
	{
		UnRegisterModuleDependences('main', 'OnBuildGlobalMenu', self::MODULE_ID, 'Dbe\CPgsoftDynamicontentredactor', 'OnBuildGlobalMenu');
		UnRegisterModuleDependences('fileman', 'OnIncludeHTMLEditorScript', self::MODULE_ID, 'Dbe\CPgsoftDynamicontentredactor', 'onIncludeHTMLEditorHandler');
		UnRegisterModuleDependences('fileman', 'OnBeforeHTMLEditorScriptsGet', self::MODULE_ID, 'Dbe\CPgsoftDynamicontentredactor', 'onBeforeHTMLEditorScriptsGet');
		UnRegisterModuleDependences('fileman', 'OnBeforeHTMLEditorScriptRuns', self::MODULE_ID, 'Dbe\CPgsoftDynamicontentredactor', 'onBeforeHTMLEditorScriptRuns');
		UnRegisterModuleDependences('main', 'OnEndBufferContent', self::MODULE_ID, 'Dbe\CPgsoftDynamicontentredactor', 'onEndBufferContent');
		UnRegisterModuleDependences('search', 'BeforeIndex', self::MODULE_ID, 'Dbe\CPgsoftDynamicontentredactor', 'onBeforeIndex');
		global $DB, $DBType, $APPLICATION;
		$this->errors = false;
		$this->errors = $DB->RunSQLBatch($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".self::MODULE_ID."/install/db/".strtolower($DB->type)."/uninstall.sql");


		return true;
	}

	function InstallEvents()
	{

		return true;
	}

	function UnInstallEvents()
	{
		return true;
	}

	function InstallFiles($arParams = array())
	{
		if (is_dir($p = $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.self::MODULE_ID.'/admin'))
		{
			if ($dir = opendir($p))
			{
				while (false !== $item = readdir($dir))
				{
					if ($item == '..' || $item == '.' || $item == 'menu.php')
						continue;
					file_put_contents($file = $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/'.self::MODULE_ID.'_'.$item,
						'<'.'? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/'.self::MODULE_ID.'/admin/'.$item.'");?'.'>');
				}
				closedir($dir);
			}
		}
		if (is_dir($p = $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.self::MODULE_ID.'/install/components'))
		{
			if ($dir = opendir($p))
			{
				while (false !== $item = readdir($dir))
				{
					if ($item == '..' || $item == '.')
						continue;
					CopyDirFiles($p.'/'.$item, $_SERVER['DOCUMENT_ROOT'].'/bitrix/components/'.$item, $ReWrite = True, $Recursive = True);
				}
				closedir($dir);
			}
		}
		if($_ENV["COMPUTERNAME"]!='BX')
		{
			CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".self::MODULE_ID."/install/js", $_SERVER["DOCUMENT_ROOT"]."/bitrix/js", true, true);
			CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".self::MODULE_ID."/install/tools", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tools", true, true);
			CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".self::MODULE_ID."/install/admin/htmleditor2", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin/htmleditor2", true, true);

		}
		return true;
	}

	function UnInstallFiles()
	{
		if (is_dir($p = $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.self::MODULE_ID.'/admin'))
		{
			if ($dir = opendir($p))
			{
				while (false !== $item = readdir($dir))
				{
					if ($item == '..' || $item == '.')
						continue;
					unlink($_SERVER['DOCUMENT_ROOT'].'/bitrix/admin/'.self::MODULE_ID.'_'.$item);
				}
				closedir($dir);
			}
		}
		if (is_dir($p = $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.self::MODULE_ID.'/install/components'))
		{
			if ($dir = opendir($p))
			{
				while (false !== $item = readdir($dir))
				{
					if ($item == '..' || $item == '.' || !is_dir($p0 = $p.'/'.$item))
						continue;

					$dir0 = opendir($p0);
					while (false !== $item0 = readdir($dir0))
					{
						if ($item0 == '..' || $item0 == '.')
							continue;
						DeleteDirFilesEx('/bitrix/components/'.$item.'/'.$item0);
					}
					closedir($dir0);
				}
				closedir($dir);
			}
		}
		if($_ENV["COMPUTERNAME"]!='BX')
		{
			DeleteDirFilesEx("/bitrix/js/".self::MODULE_ID."/");
			DeleteDirFiles($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/".self::MODULE_ID."/install/tools", $_SERVER["DOCUMENT_ROOT"]."/bitrix/tools"); // tools
		}
		return true;
	}

	function DoInstall()
	{
		global $APPLICATION;
		$this->InstallFiles();
		$this->InstallDB();
		RegisterModule(self::MODULE_ID);
	}

	function DoUninstall()
	{
		global $APPLICATION;
		$APPLICATION->includeAdminFile(
			Loc::getMessage('LANDING_UNINSTALL_TITLE'),
			$_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.self::MODULE_ID.'/install/steps/unstep1.php'
		);
		/*UnRegisterModule(self::MODULE_ID);
		$this->UnInstallDB();
		$this->UnInstallFiles();
		*/
	}
}
?>
