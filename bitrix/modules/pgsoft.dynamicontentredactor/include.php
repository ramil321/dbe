<?
/** @global string $DBType */
class Dbe {
    public static function getModuleId(){
        return basename(dirname(__FILE__));
    }
    public static function getDir(){
        return dirname(__FILE__);
    }
}

CModule::AddAutoloadClasses(
    basename(dirname(__FILE__)),
    array(
        // compability classes
        '\Dbe\DynamicEditorBlock' => "lib/dbe.php",
        '\Dbe\DynamicEditorParser' => "lib/parser.php",
        '\Dbe\DynamicEditorBlockContent' => "lib/dbe_content.php",
        '\Dbe\CPgsoftDynamicontentredactor' => "lib/main.php",
        '\Dbe\DbeFormEdit' => "lib/form_edit.php",
        '\Dbe\Tables\DbeTable' => "lib/tables/dbe.php",
        '\Dbe\Tables\DbeContentTable' => "lib/tables/dbe_content.php",
    )
);



?>
