<?php

CModule::AddAutoloadClasses(
    basename(dirname(__FILE__)),
    [
        '\Dse\Routing\Exceptions\ControllerActionExistException' => 'lib/Routing/Exceptions/ControllerActionExistException.php',
        '\Dse\Routing\Exceptions\ControllerActionTemplateExistException' => 'lib/Routing/Exceptions/ControllerActionTemplateExistException.php',
        '\Dse\Routing\Exceptions\ControllerArgumentExist' => 'lib/Routing/Exceptions/ControllerArgumentExist.php',
        '\Dse\Routing\Exceptions\ControllerException' => 'lib/Routing/Exceptions/ControllerException.php',

        '\Dse\Routing\Controllers\EditorController' => 'lib/Routing/Controllers/EditorController.php',
        '\Dse\Routing\Controllers\InstallController' => 'lib/Routing/Controllers/InstallController.php',
        '\Dse\Routing\Controllers\SnippetController' => 'lib/Routing/Controllers/SnippetController.php',

        '\Dse\Routing\Controller' => 'lib/Routing/Controller.php',
        '\Dse\Routing\ControllerManager' => 'lib/Routing/ControllerManager.php',

        '\Dse\DynamicSnippetsModule' => 'lib/DynamicSnippetsModule.php',

        '\Dse\Helpers\AbstractSingleton' => 'lib/Helpers/AbstractSingleton.php',
        '\Dse\Helpers\AbstractBitrixModuleObject' => 'lib/Helpers/AbstractBitrixModuleObject.php',

        '\Dse\Repo\AbstractRepo' => 'lib/Repo/AbstractRepo.php',
        '\Dse\Repo\IblockDataRepo' => 'lib/Repo/IblockDataRepo.php',
        '\Dse\Repo\InsertSnippetDataRepo' => 'lib/Repo/InsertSnippetDataRepo.php',
        '\Dse\Repo\SnippetsDataRepo' => 'lib/Repo/SnippetsDataRepo.php',

        '\Dse\Contracts\EditorInterface' => 'lib/Contracts/EditorInterface.php',
        '\Dse\Contracts\ParserInterface' => 'lib/Contracts/ParserInterface.php',
        '\Dse\Contracts\PatternInterface' => 'lib/Contracts/PatternInterface.php',
        '\Dse\Contracts\SnippetIblockDataInterface' => 'lib/Contracts/SnippetIblockDataInterface.php',
        '\Dse\Contracts\TemplateInterface' => 'lib/Contracts/TemplateInterface.php',

        '\Dse\Install\ModuleFileInstaller' => 'lib/Install/ModuleFileInstaller.php',
        '\Dse\Install\ModuleEvents' => 'lib/Install/ModuleEvents.php',
        '\Dse\Install\ModuleBitrixMenu' => 'lib/Install/ModuleBitrixMenu.php',

        '\Dse\Tables\SnippetTable' => 'lib/Tables/SnippetTable.php',
        '\Dse\Tables\InsertSnippetTable' => 'lib/Tables/InsertSnippetTable.php',
    ]
);
