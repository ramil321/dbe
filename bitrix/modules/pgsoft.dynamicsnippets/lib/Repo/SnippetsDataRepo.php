<?php

namespace Dse\Repo;


use Dse\Contracts\SnippetIblockDataInterface;
use Dse\Entities\SnippetData;
use Dse\Tables\SnippetTable;

class SnippetsDataRepo extends AbstractRepo implements SnippetIblockDataInterface
{
    /**
     * @var SnippetData[]
     */
    private $snippets;

    /**
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function load()
    {
        if (! isset($this->snippets)) {
            return;
        }

        $snippets = SnippetTable::getList(
            array(
                'select' => array('*')
            )
        )->fetchAll();

        foreach ($snippets as $snippet) {
            $this->snippets[$snippet['id']] = $this->snippetFactory($snippet);
        }

    }

    /**
     * @param array $data
     * @return SnippetData
     */
    private function snippetFactory($data)
    {
        return new SnippetData(
            $data['id'],
            $data['active'],
            $data['name'],
            $data['iblock_id'],
            $data['icon'],
            $data['iblock_fields'],
            $data['iblock_properties'],
            $data['template'],
            $data['created_at'],
            $data['status_at']
        );
    }

    /**
     * @return SnippetData[]
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function all()
    {
        $this->load();
        return $this->snippets;
    }

    /**
     * @param array $params
     * @return array
     */
    public function findData($params)
    {
        // TODO: Implement findData() method.
    }
}