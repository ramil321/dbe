<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 09.09.2020
 * Time: 5:01
 */

namespace Dse\Repo;


use Dse\Contracts\SnippetIblockDataInterface;

class IblockDataRepo extends AbstractRepo implements SnippetIblockDataInterface
{
    /**
     * @var array
     */
    private static $iblocksData;

    /**
     * @param int|null $iblockId
     * @param array|null $fields
     * @param array|null $props
     * @param array|null $ids
     * @return array
     */
    public function create(
        $iblockId,
        array $fields = null,
        array $props = null,
        array $ids = null
    ) {
        $arSelect = array_merge($fields, array(
            "ID",
            "IBLOCK_ID",
        ));

        $arFilter = array (
            "IBLOCK_ID" => $iblockId,
            "ACTIVE" => "Y",
            "CHECK_PERMISSIONS" => "N",
        );

        $arFilter['ID'] = $ids;
        $rsElement = \CIBlockElement::GetList(array(), $arFilter, false, false, $arSelect);
        while($obElement = $rsElement->GetNextElement())
        {
            $arItem = $obElement->GetFields();
            $arProps = $obElement->GetProperties();
            foreach($arProps as $val){
                if(in_array($val['CODE'],$props)){
                    $arItem['PROPERTY_'.$val['CODE']] = $val['VALUE'];
                    $arItem['~PROPERTY_'.$val['CODE']] = $val['~VALUE'];
                }
            }

            $arResult[] = $arItem;
        }
        self::$iblocksData[$iblockId][$ids] = $arResult;
        return $arResult;
    }

    /**
     * @param array $params
     * @return array
     */
    public function findData($params)
    {
        //$params['snippet_id']
    }

    public function getIblockCode($id)
    {

    }
}