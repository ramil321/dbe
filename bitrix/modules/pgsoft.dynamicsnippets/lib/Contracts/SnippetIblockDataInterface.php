<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 09.09.2020
 * Time: 23:55
 */

namespace Dse\Contracts;

interface SnippetIblockDataInterface
{
    /**
     * @param array $params
     * @return array
     */
    public function findData($params);
}