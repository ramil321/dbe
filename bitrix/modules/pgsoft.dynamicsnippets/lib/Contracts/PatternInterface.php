<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 09.09.2020
 * Time: 21:59
 */

namespace Dse\Contracts;

interface PatternInterface
{
    /*
     * @return string
     */
    public function getPattern();

    /**
     * @param $content
     * @return array
     */
    public function matchPattern($content);
}