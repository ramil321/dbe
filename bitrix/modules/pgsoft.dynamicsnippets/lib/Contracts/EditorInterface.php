<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 11.09.2020
 * Time: 7:15
 */

namespace Dse\Contracts;

interface EditorInterface
{

    /**
     * @return array
     */
    public function getScriptStrings();

    /**
     * @return array
     */
    public function getScriptPaths();
}