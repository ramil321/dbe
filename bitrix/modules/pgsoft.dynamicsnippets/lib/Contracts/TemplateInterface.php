<?php

namespace  Dse\Contracts;

interface TemplateInterface
{
    /**
     * @return string
     */
    public function getTemplate();
}