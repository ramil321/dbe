<?php

namespace  Dse\Contracts;

interface ParserInterface
{
    /**
     * @param string $content
     */
    public function parse($content);
}