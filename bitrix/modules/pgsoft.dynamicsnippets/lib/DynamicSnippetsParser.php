<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 09.09.2020
 * Time: 5:50
 */

namespace Dbe;

use Dbe\contracts\ParserInterface;
use Dbe\entities\SnippetData;
use Dbe\factory\EmptySnippetFactory;
use Dbe\factory\IblockSnippetFactory;
use Dbe\helpers\AbstractSingleton;
use Dbe\repo\SnippetsDataRepo;

class DynamicSnippetsParser extends AbstractSingleton implements ParserInterface
{
    /**
     * @var ParserInterface[]
     */
    private $parsers = [];

    /**
     * @return void
     */
    protected function __construct()
    {
        parent::__construct();
        $snippetsDataRepo = SnippetsDataRepo::getInstance();
        foreach ($snippetsDataRepo->all() as $snippetData) {
            $this->add($this->parserFactory($snippetData));
        }
    }

    /**
     * @param string $content
     * @return string
     */
    public function parse($content)
    {
        foreach ($this->parsers as $parser) {
            $content = $parser->parse($content);
        }
        return $content;
    }

    /**
     * @param SnippetData $snippetData
     * @return ParserInterface
     */
    private function parserFactory(SnippetData $snippetData)
    {
        if ($snippetData->getIblockId()) {
            $factory = new IblockSnippetFactory($snippetData);
        } else {
            $factory = new EmptySnippetFactory($snippetData);
        }

        return $factory->createParser();
    }

    /**
     * @param ParserInterface $parser
     */
    private function add(ParserInterface $parser)
    {
        $this->parsers[] = $parser;
    }

}