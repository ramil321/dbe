<?php
namespace  Dse\Entities;

class SnippetData
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var bool
     */
    private $active;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $iblockId;

    /**
     * @var int
     */
    private $icon;

    /**
     * @var string
     */
    private $iblockFields;

    /**
     * @var string
     */
    private $iblockProperties;

    /**
     * @var string
     */
    private $template;

    /**
     * @var string
     */
    private $created_at;

    /**
     * @var string
     */
    private $status_at;

    /**
     * @param int $id
     * @param bool $active
     * @param string $name
     * @param int $iblockId
     * @param int $icon
     * @param string $iblockFields
     * @param string $iblockProperties
     * @param string $template
     * @param string $created_at
     * @param string $status_at
     */
    public function __construct(
         $id,
         $active,
         $name,
         $iblockId,
         $icon,
         $iblockFields,
         $iblockProperties,
         $template,
         $created_at,
         $status_at
    )
    {

        $this->id = $id;
        $this->active = $active;
        $this->name = $name;
        $this->iblockId = $iblockId;
        $this->icon = $icon;
        $this->iblockFields = $iblockFields;
        $this->iblockProperties = $iblockProperties;
        $this->template = $template;
        $this->created_at = $created_at;
        $this->status_at = $status_at;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getIblockId()
    {
        return $this->iblockId;
    }

    /**
     * @return int
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @return string
     */
    public function getIblockFields()
    {
        return $this->iblockFields;
    }

    /**
     * @return string
     */
    public function getIblockProperties()
    {
        return $this->iblockProperties;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return $this->template;
    }

    /**
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @return string
     */
    public function getStatusAt()
    {
        return $this->status_at;
    }
}
