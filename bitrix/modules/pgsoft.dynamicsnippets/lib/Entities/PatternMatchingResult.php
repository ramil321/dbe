<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 09.09.2020
 * Time: 23:27
 */

namespace Dse\Entities;

class PatternMatchingResult
{
    /**
     * @var int
     */
    private $matchId;

    /**
     * @var string
     */
    private $pattern;

    /**
     * @param int $matchId
     * @param string $pattern
     */
    public function __construct($matchId, $pattern)
    {
        $this->matchId = $matchId;
        $this->pattern = $pattern;
    }

    /**
     * @return int
     */
    public function getMatchId()
    {
        return $this->matchId;
    }

    /**
     * @return string
     */
    public function getPattern()
    {
        return $this->pattern;
    }

}