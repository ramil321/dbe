<?php


namespace Dse\Editors;

class EditorNew extends AbstractEditor
{

    /**
     * @return array
     */
    public function getScriptStrings()
    {
        $scripts = [];
        foreach ($this->snippets as $snippet) {

            $scripts[] = $this->getScriptStringWithJsonParams(
                [
                    (string) self::PREFIX.$snippet->getId(),
                    (int) $snippet->getIblockId(),
                    (string) $this->iblockRepo->getIblockCode($snippet->getId()),
                    (string) $this->getBitrixFileSrc($snippet->getIcon(), 150, 16),
                    (bool) $snippet->getIblockId()
                ],
                function($encodeParams)
                {
                    return "DynamicBlockReg.addBlock(
                        {$encodeParams},
                        function(obj,content){ obj.InsertHtml(content); }
                    );";
                }
            );

            $scripts[] = $this->getScriptStringWithJsonParams(
                [
                    (string) self::PREFIX.$snippet->getId(),
                    (string) $snippet->getName(),
                    (string) $this->getBitrixFileSrc($snippet->getIcon(), 20, 20),
                    (bool) $snippet->getIblockId()
                ],
                function($encodeParams)
                {
                    return "DynamicBlockRedactorButtonsReg.addNew(
                    {$encodeParams}
                    );";
                }
            );
        }
        $scripts[] = "<script src=\"/bitrix/admin/htmleditor2/dynamicontentredactor_editor_new.js\"></script>";
        return array_merge($scripts, $this->getDefaultScriptStrings());
    }

    /**
     * @return array
     */
    public function getScriptPaths()
    {
        return [
            [
                'js/admin/htmleditor2/dynamicontentredactor_editor_new.js' =>
                "/bitrix/admin/htmleditor2/dynamicontentredactor_editor_new.js",
            ]
        ];
    }
}