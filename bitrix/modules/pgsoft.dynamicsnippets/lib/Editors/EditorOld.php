<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 11.09.2020
 * Time: 7:37
 */

namespace Dse\Editors;

class EditorOld extends AbstractEditor
{

    /**
     * @return array
     */
    public function getScriptStrings()
    {
        $scripts = [];
        foreach ($this->snippets as $snippet) {

            $scripts[] = $this->getScriptStringWithJsonParams(
                [
                    (string) self::PREFIX.$snippet->getId(),
                    (int) $snippet->getIblockId(),
                    (string) $this->iblockRepo->getIblockCode($snippet->getId()),
                    (string) $this->getBitrixFileSrc($snippet->getIcon(), 128, 128),
                    (bool) $snippet->getIblockId()
                ],
                function($encodeParams)
                {
                    return "DynamicBlockReg.addBlock(
                        {$encodeParams},
                        function(obj,content){ obj.pMainObj.PasteHtmlAtCaret(content); }
                    );";
                }
            );

            $scripts[] = $this->getScriptStringWithJsonParams(
                [
                    (string) self::PREFIX.$snippet->getId(),
                    (string) $snippet->getName(),
                    (string) $this->getBitrixFileSrc($snippet->getIcon(), 20, 20),
                    (bool) $snippet->getIblockId()
                ],
                function($encodeParams)
                {
                    return "DynamicBlockRedactorButtonsReg.add(
                    {$encodeParams}
                    );";
                }
            );
        }
        $scripts[] = "<script> DynamicBlockReg.frameInit(); </script>";
        return array_merge($scripts, $this->getDefaultScriptStrings());
    }

    /**
     * @return array
     */
    public function getScriptPaths()
    {
        return [
            [
                'js/admin/htmleditor2/dynamicontentredactor_editor.js' =>
                "/bitrix/admin/htmleditor2/dynamicontentredactor_editor.js",
            ]
        ];
    }
}