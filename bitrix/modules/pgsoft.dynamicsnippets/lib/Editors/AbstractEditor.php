<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 11.09.2020
 * Time: 7:39
 */

namespace Dse\Editors;

use Dse\Contracts\EditorInterface;
use Dse\Entities\SnippetData;
use Dse\Repo\IblockDataRepo;

abstract class AbstractEditor implements EditorInterface
{

    const PREFIX = 'block_dynamic_editor';

    /**
     * @var int
     */
    protected $iblockElementId;
    /**
     * @param int $elementId
     */

    /**
     * @var IblockDataRepo
     */
    protected $iblockRepo;


    public function __construct($iblockElementId, IblockDataRepo $iblockRepo)
    {
        $this->iblockRepo = $iblockRepo;
        $this->iblockElementId = $iblockElementId;
    }

    /**
     * @var SnippetData[]
     */
    protected $snippets = [];

    public function addSnippetData(SnippetData $snippetData)
    {
        if (!isset($this->snippets[$snippetData->getId()])) {
            $this->snippets[] = $snippetData;
        }
    }

    /**
     * @return array
     */
    protected function getDefaultScriptStrings()
    {
        return [
            "<script>DynamicBlockReg.iblockElementId = {$this->iblockElementId}</script>",
            "<script src=\"/bitrix/components/bitrix/main.lookup.input/script.js\"></script>",
            "<script src=\"/bitrix/components/bitrix/main.lookup.input/templates/iblockedit/script2.js\"></script>",
            "<script src=\"/bitrix/js/dynamicontentredactor/dynamicontentredactor.js\"></script>",
        ];
    }

    protected function getBitrixFileSrc($id, $width, $height)
    {
        $file =
            \CFile::ResizeImageGet(
                $id,
                ['width'=>$width, 'height'=>$height],
                BX_RESIZE_IMAGE_PROPORTIONAL,
                true
            );
        return $file['src'];
    }

    /**
     * @param $params
     * @param callable $callback
     * @return string
     */
    protected function getScriptStringWithJsonParams(
        $params,
        callable $callback
    ) {
        if(! is_callable($callback)) {
            return '';
        }
        $encodeParams = json_encode($params);

        return "<script>".$callback($encodeParams)."</script>";
    }
}