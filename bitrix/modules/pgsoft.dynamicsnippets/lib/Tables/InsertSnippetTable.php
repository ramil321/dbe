<?php
namespace Dse\Tables;

use Bitrix\Main\Entity;
use Bitrix\Main\Type;

class InsertSnippetTable extends Entity\DataManager
{
    public static function getFilePath()
    {
        return __FILE__;
    }

    public static function getTableName()
    {
        return 'dynamicontentredactor_insert_blocks';
    }

    public static function getMap()
    {
        return array(
            new Entity\IntegerField('id', array(
                    'primary' => true,
                    'autocomplete' => true,
                )
            ),
            new Entity\IntegerField('iblock_id', array(
                    'validation' => function(){
                        return array(
                            new Entity\Validator\Length(null, 20),
                        );
                    }
                )
            ),
            new Entity\IntegerField('iblock_element_id', array(
                    'validation' => function(){
                        return array(
                            new Entity\Validator\Length(null, 20),
                        );
                    }
                )
            ),
            new Entity\IntegerField('dynamicontentredactor_id', array(
                    'validation' => function(){
                        return array(
                            new Entity\Validator\Length(null, 20),
                        );
                    }
                )
            ),
            new Entity\IntegerField('iblock_prop', array(
                    'validation' => function(){
                        return array(
                            new Entity\Validator\Length(null, 20),
                        );
                    }
                )
            ),
            new Entity\StringField('iblock_field', array(
                    'validation' => function(){
                        return array(
                            new Entity\Validator\Length(null, 20),
                        );
                    }
                )
            ),
            new Entity\DatetimeField('created_at', array(
                    'required' => true,
                    'default_value' => new Type\DateTime,
                    'validation' => function(){
                        return array(
                            new Entity\Validator\Date(),
                        );
                    }
                )
            ),
        );
    }

}