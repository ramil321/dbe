<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 11.09.2020
 * Time: 10:55
 */

namespace Dse;

class BitrixIndex
{
    public function getBodySearch($ID, $bOverWrite=false)
    {
        if(!\CModule::IncludeModule("search"))
            return;

        global $DB;
        $ID = Intval($ID);

        static $strElementSql = false;
        if(!$strElementSql)
        {
            $strElementSql = "
                SELECT BE.ID, BE.NAME, BE.XML_ID as EXTERNAL_ID,
                    BE.PREVIEW_TEXT_TYPE, BE.PREVIEW_TEXT, BE.CODE,
                    BE.TAGS,
                    BE.DETAIL_TEXT_TYPE, BE.DETAIL_TEXT, BE.IBLOCK_ID, B.IBLOCK_TYPE_ID,
                    ".$DB->DateToCharFunction("BE.TIMESTAMP_X")." as LAST_MODIFIED,
                    ".$DB->DateToCharFunction("BE.ACTIVE_FROM")." as DATE_FROM,
                    ".$DB->DateToCharFunction("BE.ACTIVE_TO")." as DATE_TO,
                    BE.IBLOCK_SECTION_ID,
                    B.CODE as IBLOCK_CODE, B.XML_ID as IBLOCK_EXTERNAL_ID, B.DETAIL_PAGE_URL,
                    B.VERSION, B.RIGHTS_MODE, B.SOCNET_GROUP_ID
                FROM b_iblock_element BE, b_iblock B
                WHERE BE.IBLOCK_ID=B.ID
                    AND B.ACTIVE='Y'
                    AND BE.ACTIVE='Y'
                    AND B.INDEX_ELEMENT='Y'
                    ".\CIBlockElement::WF_GetSqlLimit("BE.", "N")."
                    AND BE.ID=";
        }

        $dbrIBlockElement = $DB->Query($strElementSql.$ID);

        if($arIBlockElement = $dbrIBlockElement->Fetch())
        {
            $IBLOCK_ID = $arIBlockElement["IBLOCK_ID"];
            $DETAIL_URL =
                "=ID=".urlencode($arIBlockElement["ID"]).
                "&EXTERNAL_ID=".urlencode($arIBlockElement["EXTERNAL_ID"]).
                "&IBLOCK_SECTION_ID=".urlencode($arIBlockElement["IBLOCK_SECTION_ID"]).
                "&IBLOCK_TYPE_ID=".urlencode($arIBlockElement["IBLOCK_TYPE_ID"]).
                "&IBLOCK_ID=".urlencode($arIBlockElement["IBLOCK_ID"]).
                "&IBLOCK_CODE=".urlencode($arIBlockElement["IBLOCK_CODE"]).
                "&IBLOCK_EXTERNAL_ID=".urlencode($arIBlockElement["IBLOCK_EXTERNAL_ID"]).
                "&CODE=".urlencode($arIBlockElement["CODE"]);

            static $arGroups = array();
            if(!array_key_exists($IBLOCK_ID, $arGroups))
            {
                $arGroups[$IBLOCK_ID] = array();
                $strSql =
                    "SELECT GROUP_ID ".
                    "FROM b_iblock_group ".
                    "WHERE IBLOCK_ID= ".$IBLOCK_ID." ".
                    "    AND PERMISSION>='R' ".
                    "ORDER BY GROUP_ID";

                $dbrIBlockGroup = $DB->Query($strSql);
                while($arIBlockGroup = $dbrIBlockGroup->Fetch())
                {
                    $arGroups[$IBLOCK_ID][] = $arIBlockGroup["GROUP_ID"];
                    if($arIBlockGroup["GROUP_ID"]==2) break;
                }
            }

            static $arSITE = array();
            if(!array_key_exists($IBLOCK_ID, $arSITE))
            {
                $arSITE[$IBLOCK_ID] = array();
                $strSql =
                    "SELECT SITE_ID ".
                    "FROM b_iblock_site ".
                    "WHERE IBLOCK_ID= ".$IBLOCK_ID;

                $dbrIBlockSite = $DB->Query($strSql);
                while($arIBlockSite = $dbrIBlockSite->Fetch())
                    $arSITE[$IBLOCK_ID][] = $arIBlockSite["SITE_ID"];
            }
            \Dbe\DynamicEditorParser::parse($arIBlockElement["PREVIEW_TEXT"]);
            \Dbe\DynamicEditorParser::parse($arIBlockElement["DETAIL_TEXT"]);
            $BODY =
                ($arIBlockElement["PREVIEW_TEXT_TYPE"]=="html" ?
                    \CSearch::KillTags($arIBlockElement["PREVIEW_TEXT"]) :
                    $arIBlockElement["PREVIEW_TEXT"]
                )."\r\n".
                ($arIBlockElement["DETAIL_TEXT_TYPE"]=="html" ?
                    \CSearch::KillTags($arIBlockElement["DETAIL_TEXT"]) :
                    $arIBlockElement["DETAIL_TEXT"]
                );

            static $arProperties = array();
            if(!array_key_exists($IBLOCK_ID, $arProperties))
            {
                $arProperties[$IBLOCK_ID] = array();
                $rsProperties = \CIBlockProperty::GetList(
                    array("sort"=>"asc","id"=>"asc"),
                    array(
                        "IBLOCK_ID"=>$IBLOCK_ID,
                        "ACTIVE"=>"Y",
                        "SEARCHABLE"=>"Y",
                        "CHECK_PERMISSIONS"=>"N",
                    )
                );
                while($ar = $rsProperties->Fetch())
                {
                    if(strlen($ar["USER_TYPE"])>0)
                    {
                        $arUT = \CIBlockProperty::GetUserType($ar["USER_TYPE"]);
                        if(array_key_exists("GetSearchContent", $arUT))
                            $ar["GetSearchContent"] = $arUT["GetSearchContent"];
                        elseif(array_key_exists("GetPublicViewHTML", $arUT))
                            $ar["GetSearchContent"] = $arUT["GetPublicViewHTML"];
                    }
                    $arProperties[$IBLOCK_ID][$ar["ID"]] = $ar;
                }
            }

            //Read current property values from database
            $strProperties = "";
            if(count($arProperties[$IBLOCK_ID])>0)
            {
                if($arIBlockElement["VERSION"]==1)
                {
                    $rs = $DB->Query("
                        select *
                        from b_iblock_element_property
                        where IBLOCK_ELEMENT_ID=".$arIBlockElement["ID"]."
                        AND IBLOCK_PROPERTY_ID in (".implode(", ", array_keys($arProperties[$IBLOCK_ID])).")
                    ");
                    while($ar=$rs->Fetch())
                    {
                        $strProperties .= "\r\n";
                        $arProperty = $arProperties[$IBLOCK_ID][$ar["IBLOCK_PROPERTY_ID"]];
                        if($arProperty["GetSearchContent"])
                        {
                            $strPropertiesTemp =
                                call_user_func_array($arProperty["GetSearchContent"],
                                    array(
                                        $arProperty,
                                        array("VALUE" => $ar["VALUE"]),
                                        array(),
                                    )
                                );
                            \Dbe\DynamicEditorParser::parse($strPropertiesTemp);
                            $strPropertiesTemp = \CSearch::KillTags($strPropertiesTemp);
                            $strProperties .= $strPropertiesTemp;
                        }
                        elseif($arProperty["PROPERTY_TYPE"]=='L')
                        {
                            $arEnum = \CIBlockPropertyEnum::GetByID($ar["VALUE"]);
                            if($arEnum!==false)
                                $strProperties .= $arEnum["VALUE"];
                        }
                        elseif($arProperty["PROPERTY_TYPE"]=='F')
                        {
                            $arFile = \CIBlockElement::__GetFileContent($ar["VALUE"]);
                            if(is_array($arFile))
                            {
                                $strProperties .= $arFile["CONTENT"];
                                $arIBlockElement["TAGS"] .= ",".$arFile["PROPERTIES"][\COption::GetOptionString("search", "page_tag_property")];
                            }
                        }
                        else
                        {
                            $strProperties .= $ar["VALUE"];
                        }
                    }
                }
                else
                {
                    $rs = $DB->Query("
                        select *
                        from b_iblock_element_prop_m".$IBLOCK_ID."
                        where IBLOCK_ELEMENT_ID=".$arIBlockElement["ID"]."
                        AND IBLOCK_PROPERTY_ID in (".implode(", ", array_keys($arProperties[$IBLOCK_ID])).")
                    ");
                    while($ar=$rs->Fetch())
                    {
                        $strProperties .= "\r\n";
                        $arProperty = $arProperties[$IBLOCK_ID][$ar["IBLOCK_PROPERTY_ID"]];
                        if($arProperty["GetSearchContent"])
                        {
                            $strPropertiesTemp =
                                call_user_func_array($arProperty["GetSearchContent"],
                                    array(
                                        $arProperty,
                                        array("VALUE" => $ar["VALUE"]),
                                        array(),
                                    )
                                );
                            \Dbe\DynamicEditorParser::parse($strPropertiesTemp);
                            $strPropertiesTemp = \CSearch::KillTags($strPropertiesTemp);
                            $strProperties .= $strPropertiesTemp;
                        }
                        elseif($arProperty["PROPERTY_TYPE"]=='L')
                        {
                            $arEnum = \CIBlockPropertyEnum::GetByID($ar["VALUE"]);
                            if($arEnum!==false)
                                $strProperties .= $arEnum["VALUE"];
                        }
                        elseif($arProperty["PROPERTY_TYPE"]=='F')
                        {
                            $arFile = \CIBlockElement::__GetFileContent($ar["VALUE"]);
                            if(is_array($arFile))
                            {
                                $strProperties .= $arFile["CONTENT"];
                                $arIBlockElement["TAGS"] .= ",".$arFile["PROPERTIES"][\COption::GetOptionString("search", "page_tag_property")];
                            }
                        }
                        else
                        {
                            $strProperties .= $ar["VALUE"];
                        }
                    }
                    $rs = $DB->Query("
                        select *
                        from b_iblock_element_prop_s".$IBLOCK_ID."
                        where IBLOCK_ELEMENT_ID=".$arIBlockElement["ID"]."
                    ");
                    if($ar=$rs->Fetch())
                    {
                        foreach($arProperties[$IBLOCK_ID] as $property_id=>$property)
                        {
                            if( array_key_exists("PROPERTY_".$property_id, $ar)
                                && $property["MULTIPLE"]=="N"
                                && strlen($ar["PROPERTY_".$property_id])>0)
                            {
                                $strProperties .= "\r\n";
                                if($property["GetSearchContent"])
                                {
                                    $strPropertiesTemp .=
                                        call_user_func_array($property["GetSearchContent"],
                                            array(
                                                $property,
                                                array("VALUE" => $ar["PROPERTY_".$property_id]),
                                                array(),
                                            )
                                        );
                                    \Dbe\DynamicEditorParser::parse($strPropertiesTemp);
                                    $strPropertiesTemp = \CSearch::KillTags($strPropertiesTemp);
                                    $strProperties .= $strPropertiesTemp;
                                }
                                elseif($property["PROPERTY_TYPE"]=='L')
                                {
                                    $arEnum = \CIBlockPropertyEnum::GetByID($ar["PROPERTY_".$property_id]);
                                    if($arEnum!==false)
                                        $strProperties .= $arEnum["VALUE"];
                                }
                                elseif($property["PROPERTY_TYPE"]=='F')
                                {
                                    $arFile = \CIBlockElement::__GetFileContent($ar["PROPERTY_".$property_id]);
                                    if(is_array($arFile))
                                    {
                                        $strProperties .= $arFile["CONTENT"];
                                        $arIBlockElement["TAGS"] .= ",".$arFile["PROPERTIES"][\COption::GetOptionString("search", "page_tag_property")];
                                    }
                                }
                                else
                                {
                                    $strProperties .= $ar["PROPERTY_".$property_id];
                                }
                            }
                        }
                    }
                }
            }
            $BODY .= $strProperties;
            return $BODY;
        }
    }

    public function onBeforeIndex($arFields){
        if($arFields['MODULE_ID'] == 'iblock') {
            $arFields['BODY'] = $this->getBodySearch($arFields['ITEM_ID']);
        }
        return $arFields;
    }
}