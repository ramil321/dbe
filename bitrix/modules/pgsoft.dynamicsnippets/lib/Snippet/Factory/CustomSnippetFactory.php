<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 10.09.2020
 * Time: 2:16
 */

namespace Dse\Snippet\Factory;

use Dbe\contracts\ParserInterface;
use Dbe\contracts\PatternInterface;
use Dbe\contracts\SnippetIblockDataInterface;
use Dbe\contracts\TemplateInterface;
use Dbe\entities\SnippetData;
use Dbe\parsers\EmptySnippet;
use Dbe\parsers\IblockSnippet;
use Dbe\patterns\EmptyPattern;
use Dbe\patterns\IblockPattern;
use Dbe\repo\SnippetsDataRepo;
use Dbe\Template;

class CustomSnippetFactory extends EmptySnippetFactory
{

}