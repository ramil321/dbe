<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 10.09.2020
 * Time: 2:16
 */

namespace Dse\Snippet\Factory;



use Dse\Contracts\PatternInterface;
use Dse\Contracts\SnippetIblockDataInterface;
use Dse\Contracts\TemplateInterface;
use Dse\Snippet\Parser\EmptySnippet;

class EmptySnippetFactory extends AbstractSnippetFactory
{

    /**
     * @return ParserInterface
     */
    public function createParser()
    {
        $template = $this->createTemplate();
        $pattern = $this->createPattern();
        return new EmptySnippet(
            $template,
            $pattern
        );
    }

    /**
     * @return PatternInterface
     */
    public function createPattern()
    {
        return new EmptyPattern(
            $this->snippetData->getId()
        );
    }

    /**
     * @return SnippetIblockDataInterface
     */
    public function createSnippetData()
    {
        // TODO: Implement createSnippetData() method.
    }

    /**
     * @return TemplateInterface
     */
    public function createTemplate()
    {
        return new Template($this->snippetData);
    }
}