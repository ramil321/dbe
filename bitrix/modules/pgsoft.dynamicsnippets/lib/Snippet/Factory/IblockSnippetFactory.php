<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 10.09.2020
 * Time: 2:16
 */

namespace Dse\Snippet\Factory;



use Dse\Contracts\ParserInterface;
use Dse\Contracts\PatternInterface;
use Dse\Contracts\TemplateInterface;
use Dse\Repo\SnippetsDataRepo;
use Dse\Snippet\Parser\IblockSnippet;

class IblockSnippetFactory extends AbstractSnippetFactory
{

    /**
     * @return ParserInterface
     */
    public function createParser()
    {
        $template = $this->createTemplate();
        $pattern = $this->createPattern();
        $snippetData = $this->createSnippetData();
        return new IblockSnippet(
            $template,
            $pattern,
            $snippetData,
            $this->snippetData->getIblockId()
        );
    }

    /**
     * @return PatternInterface
     */
    public function createPattern()
    {
        return new IblockPattern(
            $this->snippetData->getId()
        );
    }

    /**
     * @return SnippetIblockDataInterface
     */
    public function createSnippetData()
    {
        return SnippetsDataRepo::getInstance();
    }

    /**
     * @return TemplateInterface
     */
    public function createTemplate()
    {
        return new Template($this->snippetData);
    }
}