<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 10.09.2020
 * Time: 2:08
 */

namespace Dse\Snippet\Factory;

use Dbe\contracts\ParserInterface;
use Dbe\contracts\PatternInterface;
use Dbe\contracts\SnippetIblockDataInterface;
use Dbe\contracts\TemplateInterface;
use Dbe\entities\SnippetData;

abstract class AbstractSnippetFactory
{
    /**
     * @var SnippetData
     */
    protected $snippetData;

    public function __construct(SnippetData $snippetData)
    {
        $this->snippetData = $snippetData;
    }

    /**
     * @return ParserInterface
     */
    abstract public function createParser();

    /**
     * @return PatternInterface
     */
    abstract public function createPattern();

    /**
     * @return SnippetIblockDataInterface
     */
    abstract public function createSnippetData();

    /**
     * @return TemplateInterface
     */
    abstract public function createTemplate();
}