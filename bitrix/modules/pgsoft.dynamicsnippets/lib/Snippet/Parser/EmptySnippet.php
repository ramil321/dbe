<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 09.09.2020
 * Time: 4:17
 */

namespace Dse\Snippet\Parser;



use Dse\Contracts\PatternInterface;
use Dse\Contracts\TemplateInterface;
use Dse\Entities\PatternMatchingResult;

class EmptySnippet extends AbstractSnippet
{
    /**
     * @param TemplateInterface $template
     * @param PatternInterface $pattern
     * @param int $iblockId
     */
    public function __construct(
        TemplateInterface $template,
        PatternInterface $pattern
    ) {
        parent::__construct($template, $pattern);
    }

    /**
     * @param string $content
     * @return string
     */
    public function parse($content)
    {
        /** @var PatternMatchingResult[] $patterns */
        $patterns = $this->pattern->matchPattern($content);
        foreach ($patterns as $pattern) {
                ob_start();
                $this->executeTemplate();
                $parseResult = ob_get_clean();
                preg_replace($pattern->getPattern(), $parseResult, $content);
        }
        return $content;
    }

    /**
     * @return void
     */
    protected function executeTemplate(){
        eval("?> ".$this->template->getTemplate()." <?php ");
    }

}