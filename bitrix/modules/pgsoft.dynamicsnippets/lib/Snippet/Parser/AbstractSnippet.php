<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 09.09.2020
 * Time: 3:58
 */

namespace Dse\Snippet\Parser;


use Dse\Contracts\PatternInterface;
use Dse\Contracts\TemplateInterface;

abstract class AbstractSnippet implements ParserInterface
{
    /**
     * @var TemplateInterface
     */
    protected $template;

    /**
     * @var PatternInterface
     */
    protected $pattern;


    /**
     * @param TemplateInterface $template
     * @param PatternInterface $pattern
     */
    public function __construct(
        TemplateInterface $template,
        PatternInterface $pattern
    ) {
        $this->template = $template;
        $this->pattern = $pattern;
    }
}