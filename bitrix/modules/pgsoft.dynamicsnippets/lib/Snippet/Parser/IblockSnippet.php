<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 09.09.2020
 * Time: 4:21
 */

namespace Dse\Snippet\Parser;


use Dse\Contracts\PatternInterface;
use Dse\Contracts\SnippetIblockDataInterface;
use Dse\Contracts\TemplateInterface;
use Dse\Entities\PatternMatchingResult;

class IblockSnippet extends AbstractSnippet
{
    /**
     * @var int
     */
    private $iblockId;

    /**
     * @var SnippetIblockDataInterface
     */
    private $snippetData;

    /**
     * @param TemplateInterface $template
     * @param PatternInterface $pattern
     * @param SnippetIblockDataInterface $snippetData
     * @param int $iblockId
     */
    public function __construct(
        TemplateInterface $template,
        PatternInterface $pattern,
        SnippetIblockDataInterface $snippetData,
        $iblockId
    ) {
        parent::__construct($template, $pattern);

        $this->iblockId = $iblockId;
        $this->snippetData = $snippetData;
    }

    public function parse($content)
    {
        /** @var PatternMatchingResult[] $patterns */
        $patterns = $this->pattern->matchPattern($content);
        $foundData = $this->snippetData->findData(array_keys($patterns));
        foreach ($patterns as $pattern) {
            if (isset($foundData[$this->iblockId][$pattern->getMatchId()])) {
                ob_start();
                $this->executeTemplate($foundData[$pattern->getMatchId()]);
                $parseResult = ob_get_clean();
                preg_replace($pattern->getPattern(), $parseResult, $content);
            }
        }
        return $content;
    }

    /**
     * @param $arResult
     */
    protected function executeTemplate($arResult){
        eval("?> ".$this->template->getTemplate()." <?php ");
    }

}