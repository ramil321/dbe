<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 09.09.2020
 * Time: 14:45
 */

namespace Dse\Snippet;


use Dse\Contracts\TemplateInterface;

class Template implements TemplateInterface
{
    /**
     * @var string
     */
    private $templateString;

    /**
     * @param $templateString
     */
    public function __construct($templateString)
    {
        $this->templateString = $templateString;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return $this->templateString;
    }
}