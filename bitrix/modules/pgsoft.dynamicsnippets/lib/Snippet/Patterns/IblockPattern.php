<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 09.09.2020
 * Time: 22:29
 */

namespace Dse\Snippet\Patterns;


use Dse\Entities\PatternMatchingResult;

class IblockPattern extends AbstractPattern
{
    /**
     * @return string
     */
    public function getPattern()
    {
        return '/<img[^>]*class="'.$this->id.' dbe_id_(\d+?)"(.*?)>/';
    }

    /**
     * @param int $id
     * @return string
     */
    private function getPatternWithId($id)
    {
        return '/<img[^>]*class="'.$this->id.' dbe_id_'.$id.'"(.*?)>/';
    }

    /**
     * @param $content
     * @return PatternMatchingResult[]
     */
    public function matchPattern($content)
    {
        $result = [];
        $ids = preg_match_all($this->getPattern(), $content, $matches);
        foreach ($ids as $id) {
            $result[$id] = new PatternMatchingResult(
                $id,
                $this->getPatternWithId($id)
            );
        }
        return $result;
    }

}