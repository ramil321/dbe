<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 09.09.2020
 * Time: 22:29
 */

namespace Dse\Snippet\Patterns;


use Dse\Entities\PatternMatchingResult;

class EmptyPattern extends AbstractPattern
{

    /**
     * @return string
     */
    public function getPattern()
    {
        return '/<img[^>]*class="'.$this->id.' dbe_id_empty"(.*?)>/';
    }

    /**
     * @param $content
     * @return PatternMatchingResult[]
     */
    public function matchPattern($content)
    {
        return [
            $this->id => new PatternMatchingResult(
                $this->id,
                $this->getPattern()
            )
        ];
    }
}