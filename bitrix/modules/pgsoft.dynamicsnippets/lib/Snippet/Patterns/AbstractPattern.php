<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 09.09.2020
 * Time: 23:44
 */

namespace Dse\Snippet\Patterns;


use Dse\Contracts\PatternInterface;

abstract class AbstractPattern implements PatternInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @param int $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }
}