<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 10.09.2020
 * Time: 1:49
 */

namespace Dse\Snippet\Patterns;


use Dse\Entities\PatternMatchingResult;

class CustomPattern extends AbstractPattern
{

    /**
     * @var string
     */
    private $pattern;

    public function __construct($id, $pattern)
    {
        parent::__construct($id);
        $this->pattern = $pattern;
    }

    public function getPattern()
    {
        return $this->pattern;
    }

    /**
     * @param $content
     * @return array
     */
    public function matchPattern($content)
    {
        return [
            $this->id => new PatternMatchingResult(
                $this->id,
                $this->getPattern()
            )
        ];
    }


}