<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 20.09.2020
 * Time: 14:49
 */

namespace Dse\Install;

use Dse\ModuleBitrixMenu;
use Dse\DynamicSnippetsModule;
use Dse\Helpers\AbstractBitrixModuleObject;
use Exception;

class ModuleEvents extends AbstractBitrixModuleObject
{
    private function getEvents()
    {
        $events = [
            [
                'main',
                'OnBuildGlobalMenu',
                $this->moduleId,
                get_class($this),
                'onBuildGlobalMenu'
            ],
            [
                'fileman',
                'OnIncludeHTMLEditorScript',
                $this->moduleId,
                get_class($this),
                'onIncludeHTMLEditorHandler'
            ],
            [
                'fileman',
                'OnBeforeHTMLEditorScriptsGet',
                $this->moduleId,
                get_class($this),
                'onBeforeHTMLEditorScriptsGet'
            ],
            [
                'fileman',
                'OnBeforeHTMLEditorScriptRuns',
                $this->moduleId,
                get_class($this),
                'onBeforeHTMLEditorScriptRuns'
            ],
        ];
        return $events;
    }

    public function registerModuleDependences()
    {

        foreach ($this->getEvents() as $event) {
            RegisterModuleDependences(
                $event[0],
                $event[1],
                $event[2],
                $event[3],
                $event[4]
            );
        }
    }

    public function removeModuleDependences()
    {
        foreach ($this->getEvents() as $event) {
            RegisterModuleDependences(
                $event[0],
                $event[1],
                $event[2],
                $event[3],
                $event[4]
            );
        }
    }

    /**
     * События битрикса
    */
    public function onBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu)
    {
        foreach (DynamicSnippetsModule::getInstance()->getMenu() as $menu) {
            $aModuleMenu[] = $menu;
        }
    }
    public static function onEndBufferContent(&$content)
    {
        /* global $APPLICATION;
         $page = $APPLICATION->GetCurPage();
         $pos = strpos($page, '/bitrix/admin');
         if ($pos === false) {
             \Dbe\DynamicEditorParser::parse($content);
         }*/
    }

    /**
     * @throws Exception
     */
    public static function onIncludeHTMLEditorHandler()
    {
        global $APPLICATION;
        \CJSCore::Init(array("jquery"));
        foreach (DynamicSnippetsModule::getInstance()->getEditor('old')->getScriptStrings() as $scriptString) {
            $APPLICATION->AddHeadString($scriptString,true);
        }
    }
    public static function onBeforeHTMLEditorScriptsGet($editorName, $arEditorParams)
    {
        return [
            'JS' => [ 'dynamicontentredactor_editor.js' ]
        ];
    }

    /**
     * @throws Exception
     */
    public static function onBeforeHTMLEditorScriptRuns()
    {
        global $APPLICATION;
        \CJSCore::Init(array("jquery"));
        foreach (DynamicSnippetsModule::getInstance()->getEditor('new')->getScriptStrings() as $scriptString) {
            $APPLICATION->AddHeadString($scriptString,true);
        }
    }
}