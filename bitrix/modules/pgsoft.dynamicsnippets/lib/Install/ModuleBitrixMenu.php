<?php


namespace Dse\Install;


use Bitrix\Catalog\Controller\Controller;
use Dse\DynamicSnippetsModule;
use Dse\Helpers\AbstractBitrixModuleObject;
use Dse\Routing\ControllerManager;

class ModuleBitrixMenu extends AbstractBitrixModuleObject
{
    /**
     * @return array
     */
    public function getMenuList()
    {
        if ($GLOBALS['APPLICATION']->GetGroupRight("main") < "R") {
            return [];
        }

        $aModuleMenu[] = [
            "parent_menu" => "global_menu_settings",
            "section" => $this->moduleId,
            "sort" => 50,
            "text" => 'Динамические снипеты в инфоблоках',
            "title" => 'Динамические снипеты в инфоблоках',
            "url" => DynamicSnippetsModule::getInstance()
                ->getControllerManager()
                ->createUrl('editor', 'index'),
            "icon" => "",
            "page_icon" => "",
            "items_id" => $this->moduleId."_items"
        ];
        return $aModuleMenu;
    }

}