<?php


namespace Dse\Install;


use Dse\Helpers\AbstractBitrixModuleObject;
use Exception;

class ModuleFileInstaller extends AbstractBitrixModuleObject
{

    /**
     * @var string
     */
    private $modulePath;

    /**
     * @var string
     */
    private $installPath;

    /**
     * @var string
     */
    private $bitrixAdminPath;


    /**
     * ModuleFileIntaller constructor.
     * @param $moduleId
     * @param $path
     * @throws Exception
     */
    public function __construct($moduleId)
    {
        parent::__construct($moduleId);
        $this->bitrixAdminPath = $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin';
        $modulePath = $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$moduleId;
        if (!is_dir ( $modulePath )) {
            throw new Exception('Module directory not find');
        }
        $this->installPath = $modulePath.'/install';
        $this->modulePath = $modulePath;
    }

    public function installFiles()
    {

        $this->readDir($this->modulePath.'/admin', function($item) {
            if ($item == '..' || $item == '.' || $item == 'menu.php') {
                return true;
            }
            file_put_contents($file = $this->bitrixAdminPath.'/'.$this->moduleId.'_'.$item,
                '<'.'? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/'.$this->moduleId.'/admin/'.$item.'");?'.'>');
        });

        if($_ENV["COMPUTERNAME"]!='BX')
        {
            CopyDirFiles(
                $this->installPath."/js",
                $_SERVER["DOCUMENT_ROOT"]."/bitrix/js",
                true,
                true
            );
            CopyDirFiles(
                $this->installPath."/tools",
                $_SERVER["DOCUMENT_ROOT"]."/bitrix/tools",
                true,
                true
            );
            CopyDirFiles(
                $this->installPath."/admin/htmleditor2",
                $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin/htmleditor2",
                true,
                true
            );

        }
        return true;
    }

    public function unInstallFiles()
    {

        $this->readDir($this->bitrixAdminPath.'/admin', function($item, $dir) {
            if ($item == '..' || $item == '.') {
                return true;
            }
            unlink($this->bitrixAdminPath.'/'.$this->moduleId.'_'.$item);
        });
        return true;
    }

    private function readDir($dir, callable $actionFileCallback)
    {
        if (!is_dir($p = $dir))
        {
            return;
        }

        $dir = opendir($p);

        if (!$dir)
        {
            return;
        }

        while (false !== $item = readdir($dir))
        {
            if($actionFileCallback($item, $dir)) {
                continue;
            }
        }

        closedir($dir);
    }
}