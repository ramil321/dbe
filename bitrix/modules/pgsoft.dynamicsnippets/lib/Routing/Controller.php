<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 11.09.2020
 * Time: 12:53
 */

namespace Dse\Routing;


use Bitrix\Main\HttpRequest;
use Dse\Helpers\AbstractBitrixModuleObject;
use Dse\Routing\Exceptions\ControllerActionExistException;
use Dse\Routing\Exceptions\ControllerActionTemplateExistException;

abstract class Controller extends AbstractBitrixModuleObject
{
    /**
     * @var HttpRequest
     */
    protected $request;

    /**
     * Controller constructor.
     * @param string $moduleId
     * @param HttpRequest $request
     */
    public function __construct($moduleId, HttpRequest $request)
    {
        parent::__construct($moduleId);
        $this->request = $request;
    }

    /**
     * @param string $path
     * @param array $params
     * @throws ControllerActionTemplateExistException
     */
    protected function render($path, $params = null)
    {
        if (! isset($params)) {
            $params = [];
        }
        $filePath = $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.$this->moduleId.'/views/'.$path;
        if (file_exists($filePath)) {
            $this->includeAdminFile($filePath, $params);
        } else {
            throw new ControllerActionTemplateExistException();
        }
    }

    /**
     * @param array $params
     * @throws ControllerActionExistException
     */
    protected function json($params)
    {
        if (!is_array($params)) {
            throw new ControllerActionExistException();
        }
        echo(json_encode($params));
    }

    /**
     * @param $name
     * @param $params
     * @return mixed
     * @throws ControllerActionExistException
     * @throws \ReflectionException
     */
    public function __call($name, $params)
    {
        $reflection = new \ReflectionClass($this);
        if (!$reflection->hasMethod($name)) {
            throw new ControllerActionExistException();
        }
        return call_user_func_array([$this, $name], $params);
    }

    /**
     * @param string $filepath
     * @param array $params
     */
    private function includeAdminFile($filepath, $params)
    {
        $keys = array_keys($GLOBALS);
        $keys_count = count($keys);
        for($i=0; $i<$keys_count; $i++)
            if($keys[$i]!="i" && $keys[$i]!="GLOBALS" && $keys[$i]!="strTitle" && $keys[$i]!="filepath")
                global ${$keys[$i]};

        include($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/prolog_admin_after.php");
        extract($params);
        include($filepath);
        include($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/include/epilog_admin.php");
        die();
    }

}