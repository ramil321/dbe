<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 19.09.2020
 * Time: 10:57
 */

namespace Dse\Routing\Exceptions;

class ControllerArgumentExist extends ControllerException
{
    protected $message = 'Request argument exist';
}