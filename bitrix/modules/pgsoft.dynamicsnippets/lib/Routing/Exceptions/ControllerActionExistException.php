<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 19.09.2020
 * Time: 3:47
 */

namespace Dse\Routing\Exceptions;

class ControllerActionExistException extends ControllerException
{
    protected $code = 404;
    protected $message = 'Page Not Found';
}