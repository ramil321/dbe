<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 19.09.2020
 * Time: 3:59
 */

namespace Dse\Routing\Exceptions;

class ControllerActionTemplateExistException extends ControllerException
{
    protected $message = 'Template exist';
}