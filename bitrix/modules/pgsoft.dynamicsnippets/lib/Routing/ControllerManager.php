<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 19.09.2020
 * Time: 10:42
 */

namespace Dse\Routing;

use Bitrix\Main\Context;
use Dse\Helpers\AbstractBitrixModuleObject;
use Dse\Helpers\AbstractSingleton;

class ControllerManager extends AbstractBitrixModuleObject
{
    /**
     * @var \Bitrix\Main\HttpRequest
     */
    protected $request;

    /**
     * @var array
     */
    private $controllerNames;

    /**
     * ControllerManager constructor.
     * @param string $moduleId
     */
    public function __construct($moduleId)
    {
        parent::__construct($moduleId);
        $this->controllerNames = $this->getClassesInNamespace();
        $this->request = Context::getCurrent()->getRequest();
    }

    public function route()
    {
        /**
         * if(!$isAdmin = $USER->IsAdmin())
        $APPLICATION->AuthForm();
         */

        if (isset($this->controllerNames[$this->request->get('type')])) {
            $controllerClass = $this->controllerNames[$this->request->get('type')];
            $controller = new $controllerClass($this->moduleId, $this->request);
            $controller->{$this->request->get('action')}();
        }
    }

    /**
     * @param string $type
     * @param string $action
     * @return string
     */
    public function createUrl($type, $action)
    {
        return $this->getBaseUrl().'?'.http_build_query([
            'type' => $type,
            'action' => $action,
        ]);
    }

    protected function getBaseUrl()
    {
        return '/bitrix/admin/'.$this->moduleId.'_index.php';
    }

    /**
     * @return array
     */
    private function getClassesInNamespace()
    {
        $files = scandir(dirname(__FILE__).'/controllers');
        $classes = [];
        foreach ($files as $file) {
            $key = strtolower( str_replace('Controller.php', '', $file) );
            $classes[$key] =  __NAMESPACE__ . '\\Controllers\\' . str_replace('.php', '', $file);
        }

        return array_filter($classes, function($possibleClass) {
            return class_exists($possibleClass);
        });
    }
}