<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 19.09.2020
 * Time: 2:58
 */

namespace Dse\Routing\Controllers;

use Dse\Routing\Controller;
use Dse\Routing\Exceptions\ControllerActionTemplateExistException;

class EditorController extends Controller
{
    /**
     * @throws ControllerActionTemplateExistException
     */
    public function index()
    {
        $this->render('snippet-index.php');
    }

    /**
     * @throws ControllerActionTemplateExistException
     */
    public function edit()
    {
        $this->render('snippet-edit.php');
    }
}
