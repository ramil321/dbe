<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 11.09.2020
 * Time: 12:34
 */

namespace Dse;


use Bitrix\Main\Context;
use Dse\Install\ModuleBitrixMenu;
use Dse\Install\ModuleEvents;
use Dse\Install\ModuleFileInstaller;
use Dse\Contracts\EditorInterface;
use Dse\Editors\AbstractEditor;
use Dse\Helpers\AbstractSingleton;
use Dse\Repo\IblockDataRepo;
use Dse\Routing\ControllerManager;
use Exception;

class DynamicSnippetsModule extends AbstractSingleton
{
    const MODULE_ID = 'pgsoft.dynamicsnippets';

    const EDITORS = [
        'new' => '\Dse\Editors\EditorNew',
        'old' => '\Dse\Editors\EditorOld',
    ];

    /**
     * @var IblockDataRepo
     */
    private $iblockDataRepo;

    /**
     * @var \Bitrix\Main\HttpRequest
     */
    private $request;

    /**
     * @var ControllerManager
     */
    private $controllerManager;


    /**
     * DynamicSnippetsModule constructor.
     */
    protected function __construct()
    {
        parent::__construct();
        $this->iblockDataRepo = IblockDataRepo::getInstance();
        $this->request = Context::getCurrent()->getRequest();
        $this->controllerManager = new ControllerManager(self::MODULE_ID);
    }


    /**
     * @return array
     */
    public function getMenu()
    {
        return (new ModuleBitrixMenu(self::MODULE_ID))->getMenuList();
    }

    /**
     * @param string $editor
     * @return EditorInterface
     * @throws Exception
     */
    public function getEditor($editor)
    {
        if (self::EDITORS[$editor] instanceof EditorInterface) {
            throw new Exception('Editor exist');
        }

        if (!class_exists(self::EDITORS[$editor])) {
            throw new Exception('Editor class exist');
        }

        $iblockElementId = $this->request->get('ELEMENT_ID');
        /** @var AbstractEditor $editorClassName */
        $editorClassName = self::EDITORS[$editor];
        return new $editorClassName($iblockElementId, $this->iblockDataRepo);
    }


    /**
     * @return EditorInterface[]
     * @throws Exception
     */
    public function getEditors()
    {
        $editors = [];
        foreach (self::EDITORS as $editor) {
            $editors[] = $editor;
        }
        return $editors;
    }

    public function getEvents()
    {
        return new ModuleEvents(self::MODULE_ID);
    }

    /**
     * @return ModuleFileInstaller
     * @throws Exception
     */
    public function getFileInstaller()
    {
        return new ModuleFileInstaller(self::MODULE_ID);
    }

    /**
     * @return ControllerManager
     */
    public function getControllerManager()
    {
        return $this->controllerManager;
    }
}