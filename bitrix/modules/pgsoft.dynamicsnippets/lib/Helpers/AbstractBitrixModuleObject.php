<?php
/**
 * Created by PhpStorm.
 * User: ramil
 * Date: 20.09.2020
 * Time: 14:53
 */

namespace Dse\Helpers;

abstract class AbstractBitrixModuleObject
{
    /**
     * @var string
     */
    protected $moduleId;

    /**
     * @param $moduleId
     */
    public function __construct($moduleId)
    {
        $this->moduleId = $moduleId;
    }
}