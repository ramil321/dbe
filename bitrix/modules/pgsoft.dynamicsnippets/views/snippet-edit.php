<?php
\CJSCore::Init(array("jquery"));
CModule::includeModule('pgsoft.dynamicontentredactor');
$APPLICATION->AddHeadString('<script src="/bitrix/js/dynamicontentredactor/ace.js"></script>',true);
$APPLICATION->AddHeadString('<script src="/bitrix/js/dynamicontentredactor/theme-chrome.js"></script>',true);
$APPLICATION->AddHeadString('<script src="/bitrix/js/dynamicontentredactor/mode-php.js"></script>',true);
$APPLICATION->AddHeadString('<script src="/bitrix/js/dynamicontentredactor/worker-php.js"></script>',true);
$APPLICATION->AddHeadString('<script src="/bitrix/js/dynamicontentredactor/dynamicontentredactor_edit_form.js"></script>',true);


//$arResult = \Dbe\DbeFormEdit::execEdit();
//\Dbe\DynamicEditorBlock::getEl($arResult['FORM_FIELDS']['iblock_id'],$arResult['FORM_FIELDS']['iblock_fields'],$arResult['FORM_FIELDS']['iblock_properties']);

require($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/include/prolog_admin_after.php");

$aTabs = array(
    array("DIV"=>"tab1", "TAB"=>$arResult['TITLES']['TAB_NAME'], "ICON"=>"main_user_edit", "TITLE"=>$arResult['TITLES']['TAB_NAME']),
);
$APPLICATION->SetTitle($arResult['TITLES']['TAB_NAME']);
$editTab = new CAdminTabControl("editTab", $aTabs);
$strError = '';
foreach($arResult['ERRORS'] as $val){
    $strError .= $val.' ';
}

CAdminMessage::ShowOldStyleError($strError);
?>
<form action="<?echo $APPLICATION->GetCurUri()?>" method="POST" enctype="multipart/form-data">
    <input type="hidden" name="action" value="<?=$action?>">
    <input type="hidden" name="id" value="<?=$arResult['FORM_FIELDS']['id']?>">
    <?
    echo bitrix_sessid_post();
    $editTab->Begin();
    $editTab->BeginNextTab();
    ?>
    <tr class=heading>
        <td colspan=2>Изменить блок</td>
    </tr>
    <tr>
        <td><span class="adm-required-field">Название</span></td>
        <td><input type="text" name="name" value="<?=$arResult['FORM_FIELDS']['name']?>"></td>
    </tr>
    <tr>
        <td><span class="adm-required-field">Иконка в визуальном редакторе</span></td>
        <td>
            <? $file = CFile::ResizeImageGet($arResult['FORM_FIELDS']['icon'], array('width'=>64, 'height'=>64), BX_RESIZE_IMAGE_EXACT, true); ?>
            <image src="<?=$file['src']?>" /><br/>
            <?echo CFile::InputFile("icon", 20, 100, $arResult['FORM_FIELDS']['icon']);  ?>
            <style>
                .bx-input-file-desc{
                    display: none;
                }
            </style>
        </td>
    </tr>
    <tr>
        <td>Выберите тип инфоблока</td>
        <td>
            <select id="dbe_edit_iblock_type">
                <option value="">-</option>
                <?foreach($arResult['IBLOCK_TYPES'] as $key=>$val):?>
                    <option <?if($val['ACTIVE']):?>selected<?endif?> value="<?=$key?>"><?=$val['VAL']?></option>
                <?endforeach?>
            </select>
        </td>
    </tr>
    <tr>
        <td>Выберите инфоблок</td>
        <td>
            <select name="iblock_id" id="dbe_edit_iblock_id">
                <option  id="dbe_edit_iblock_id_default" value="">-</option>
                <?foreach($arResult['IBLOCKS'] as $key=>$val):?>
                    <option <?if($val['ACTIVE']):?>selected<?endif?> <?if($arResult['IBLOCK_TYPE'] != $val['TYPE'] && $arResult['IBLOCK_TYPE']):?>style="display:none;"<?endif?> class="dbe_edit_iblock_id_<?=$val['TYPE']?>" value="<?=$key?>"><?=$val['VAL']?></option>
                <?endforeach?>
            </select>
        </td>
    </tr>
    <tr>
        <td>Поля элемента инфоблока</td>
        <td>
            <select size="4" multiple name="iblock_fields[]" id="dbe_edit_iblock_fields">
                <?foreach($arResult['IBLOCK_FIELDS'] as $key=>$val):?>
                    <option <?if($val['ACTIVE']):?>selected<?endif?> value="<?=$key?>"><?=$val['VAL']?></option>
                <?endforeach?>
            </select>

        </td>
    </tr>
    <tr>
        <td>Свойства инфоблока</td>
        <td>
            <select size="4" multiple name="iblock_properties[]" id="dbe_edit_iblock_props">
                <?foreach($arResult['IBLOCK_PROPERTIES'] as $key=>$val):?>
                    <option data-multiple="<?=$val['MULTIPLE']?>" <?if($val['ACTIVE']):?>selected<?endif?> value="<?=$key?>"><?=$val['VAL']?></option>
                <?endforeach?>
            </select>
        </td>
    </tr>
    <tr>
        <td valign=top>Доступные поля</td>
        <td></td>
    </tr>
    <tr>
        <td colspan="2" id="dbe_edit_available_fields"></td>
    </tr>

    <tr>
        <td valign=top><span class="adm-required-field">Шаблон</span><br></td>
    </tr>
    <tr>
        <td colspan="2"  align="center">
            <div class="container-code">
                <div id="dbe_edite_template"></div>
            </div>
        </td>
        <textarea style="display: none"  name="template" cols=80 rows=10><?=$arResult['FORM_FIELDS']['template']?></textarea>

    </tr>
    <?
    $editTab->Buttons();
    ?>
    <input type="submit" name=save value="Сохранить">
    <?
    $editTab->End();
    ?>
</form>
<style type="text/css">
    #dbe_edite_template {
        width: 100%;
        height: 500px;
    }
</style>
<script>
    $(function () {
        DbeEditForm.init();
    });
</script>

