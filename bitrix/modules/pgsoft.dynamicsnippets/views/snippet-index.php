<?php

use Dse\DynamicSnippetsModule;
use Dse\Tables\SnippetTable;

//\Dse\DynamicSnippetsModule::getInstance()->getEditor()
global $APPLICATION;
$APPLICATION->AddHeadString('<script src="/bitrix/js/dynamicontentredactor/dynamicontentredactor_edit_form.js"></script>',true);
$APPLICATION->SetTitle('Список снипетов');


//\Dbe\DbeFormEdit::execEdit();
$sTableID = SnippetTable::getTableName();
$oSort = new CAdminSorting($sTableID, "sort", "asc");
$lAdmin = new CAdminList($sTableID, $oSort);

if(($arID = $lAdmin->GroupAction()) && $isAdmin)
{
    if($_REQUEST['action_target']=='selected')
    {
        $arID = Array();
        // $rsData = CWizardUtil::GetWizardList(false, true);
        // while($arRes = $rsData->Fetch())
        //    $arID[] = $arRes['ID'];
    }

    foreach($arID as $ID)
    {
        if(strlen($ID)<=0)
            continue;

        switch($_REQUEST['action'])
        {
            case "delete":
                @set_time_limit(0);
                //     if(!CWizardUtil::DeleteWizard($ID))
                //         $lAdmin->AddGroupError(GetMessage("MAIN_WIZARD_DELETE_ERROR"), $ID);
                break;
            case "export":
                ?>
                <script type="text/javascript">
                    exportWizard('<?=$ID?>');
                </script>
                <?
                break;
            /*case "copy":
                CWizardUtil::CopyWizard($ID, $ID."_copy");
                break;*/
        }
    }
}

if($_REQUEST['action'] == 'delete') {
    global $APPLICATION;
    $APPLICATION->includeAdminFile(
        'Удалить снипет и конвертировать динамический контент в HTML',
        $_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/pgsoft.dynamicontentredactor/install/steps/unstep1.php'
    );
}

$rsData = new CDBResult;
//$rsData->InitFromArray(CWizardUtil::GetWizardList(false, true));
$rsData = new CAdminResult(/*\Dbe\Tables\DbeTable::getList()*/[], $sTableID);
$rsData->NavStart();
$lAdmin->NavText($rsData->GetNavPrint(GetMessage("PAGES"), false));

$lAdmin->AddHeaders(
    Array(
        Array("id"=>"id", "content"=>"ID", "default"=>true),
        Array("id"=>"name", "content"=> 'Название', "default"=>true)
    )
);

while($arRes = $rsData->NavNext(true, "f_"))
{

    $row =& $lAdmin->AddRow($f_ID, $arRes);

    $idTmp = $f_ID;
    $arID = explode(":", $f_ID);
    if (count($arID) > 2)
        $idTmp = $arID[1].":".$arID[2];

    $row->AddField("ID", $idTmp);

    $arActions = Array();
    /*if ($isAdmin)
    {
        $startType = (array_key_exists("START_TYPE",$arRes) ? $arRes["START_TYPE"] : "POPUP");
        $startType = strtoupper($startType);

        if ($startType == "POPUP")
            $arActions[] = array("DEFAULT" => "Y", "ICON"=>"install", "TEXT" => GetMessage("MAIN_WIZARD_ADMIN_INSTALL"), "ACTION"=>"WizardWindow.Open('".$f_ID."','".bitrix_sessid()."')");
        else if ($startType == "WINDOW")
            $arActions[] = Array(
                "DEFAULT" => "Y",
                "ICON"=>"install",
                "TEXT" => GetMessage("MAIN_WIZARD_ADMIN_INSTALL"),
                "ACTION"=>"window.open('wizard_install.php?lang=".LANGUAGE_ID."&wizardName=".$f_ID."&".bitrix_sessid_get()."');"
            );
    }

    if (count($arID) <= 2)
        $arActions[] = array("ICON"=>"export", "TEXT"=>GetMessage("MAIN_WIZARD_ADMIN_DOWNLOAD"), "ACTION"=>"exportWizard('".$f_ID."')");

    if ($isAdmin && (count($arID) <= 2))
    {
        $arActions[] = Array("SEPARATOR"=>true);
        $arActions[] = Array(
            "ICON"=>"delete",
            "TEXT"=>GetMessage("MAIN_ADMIN_MENU_DELETE"),
            "ACTION"=>"if(confirm('".GetMessage('MAIN_ADMIN_MENU_DELETE_CONF')."')) ".$lAdmin->ActionDoGroup($f_ID, "delete")
        );
    }*/

    $arActions[] = Array(
        "DEFAULT" => "Y",
        "ICON"=>"edit",
        "TEXT" => 'Изменить',
        "ACTION"=>"window.location.href = 'pgsoft.dynamicontentredactor_dbe.php?page=element&lang=".LANGUAGE_ID."&action=edit&id=".$f_id."&".bitrix_sessid_get()."';"
    );
    $arActions[] = Array(
        "DEFAULT" => "Y",
        "ICON"=>"delete",
        "TEXT" => 'Удалить',
        "ACTION"=>"window.location.href = 'pgsoft.dynamicontentredactor_dbe.php?page=list&lang=".LANGUAGE_ID."&action=delete&id=".$f_id."&".bitrix_sessid_get()."';"
    );
    $row->AddActions($arActions);
}

/*
$groupAction = Array(
	"copy" => GetMessage("MAIN_ADMIN_MENU_COPY"),
	"delete" => GetMessage("MAIN_ADMIN_MENU_DELETE"),
);
$lAdmin->AddGroupActionTable($groupAction);
*/

$arContext = array(
    array(
        "TEXT"	=> "Добавить",
        "LINK"	=> DynamicSnippetsModule::getInstance()->getControllerManager()->createUrl('editor', 'edit'),
        "TITLE"	=> "Добавить",
        "ICON"	=> "btn_new"
    ),
);
$lAdmin->AddAdminContextMenu($arContext);

$lAdmin->CheckListMode();


 ?>

    <script type="text/javascript">
        <!--
        function exportWizard(val)
        {
            window.open("wizard_export.php?ID="+val+"&<?=bitrix_sessid_get()?>");
        }
        //-->
    </script>

<?$lAdmin->DisplayList();?>
