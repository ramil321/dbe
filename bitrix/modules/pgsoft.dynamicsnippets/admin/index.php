<?php

use Dse\DynamicSnippetsModule;

error_reporting(E_ALL);
ini_set('display_errors','On');

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
CModule::includeModule('pgsoft.dynamicsnippets');


DynamicSnippetsModule::getInstance()->getControllerManager()->route();
