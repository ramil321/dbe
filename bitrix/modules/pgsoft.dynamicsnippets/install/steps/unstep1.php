<?php
use \Bitrix\Main\Localization\Loc;
\CJSCore::Init(array("jquery"));
$APPLICATION->AddHeadString('<script src="/bitrix/js/dynamicontentredactor/dynamicontentredactor_clear_content.js"></script>',true);
?>


<form id="byte_stats_form" action="<?=$APPLICATION->GetCurUri()?>" method="get">
    <?= bitrix_sessid_post() ?>
    <input type="hidden" name="delete_id" value="<?= $_REQUEST['id']?>"/>

    <div id="progress_bar" class="adm-info-message-wrap adm-info-message-gray">
        <div class="adm-info-message">
            Конвертация снипетов.
            <div class="adm-progress-bar-outer" style="width: 500px;">
                <div class="adm-progress-bar-inner" style="width: 0px;">
                    <div class="adm-progress-bar-inner-text" style="width: 500px;"></div>
                </div>
            </div>
            <div class="adm-info-message-buttons"></div>
        </div>
    </div>


    <?/*	<tr>
		<input type=button onClick="ExportVote.exec()" value="Сгенерировать отчет" id="gen" class="adm-btn-save">
	</tr>
	*/?>

    <div style="padding-top: 10px;" id="result"></div>
    <?
        if($_REQUEST['action'] == 'delete') {
            $message = 'Удалить снипет и конвертировать все используемые снипеты в html';
        } else {
            $message = 'Удалить модуль и конвертировать все снипеты в html';
        }
    ?>
    <input
            onclick="ExportVote.exec()"
            type="button"
            class="adm-btn-save"
            id="save_stats"
            title="<?=$message?>"
            value="<?=$message?>"
            name="Apply"
    >

</form>

