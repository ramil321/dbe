<?

use Bitrix\Main\Localization\Loc;
use Dse\ModuleBitrixMenu;
use Dse\DynamicSnippetsModule;


IncludeModuleLangFile(__FILE__);
Class pgsoft_dynamicsnippets extends CModule
{

	var $MODULE_ID;
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $strError = '';
	private $type = 'mysql';
	function __construct()
	{
	    $this->includeAutoload();

	    $this->MODULE_ID = \Dse\DynamicSnippetsModule::MODULE_ID;
		/** @var array $arModuleVersion */
		include($this->currentDirPath() . "/version.php");
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage("pgsoft.dynamicontentredactor_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("pgsoft.dynamicontentredactor_MODULE_DESC");
		$this->PARTNER_NAME = GetMessage("pgsoft.dynamicontentredactor_PARTNER_NAME");
		$this->PARTNER_URI = GetMessage("pgsoft.dynamicontentredactor_PARTNER_URI");
	}

	private function currentDirPath()
	{
		return dirname( __FILE__ );
	}

    /**
     * @throws Exception
     */
    public function installEditorScript()
	{
		foreach (\Dse\DynamicSnippetsModule::getInstance()->getEditors() as $editor) {
            foreach ($editor->getScriptPaths() as $from=>$to) {
                $script = file_get_contents($this->currentDirPath().$from);
                file_put_contents($_SERVER['DOCUMENT_ROOT'].$to, $script);
            }
		}
	}

	public function installEvents()
	{
		\Dse\DynamicSnippetsModule::getInstance()->getEvents()->registerModuleDependences();
	}

	public function unistallEvents()
	{
		\Dse\DynamicSnippetsModule::getInstance()->getEvents()->removeModuleDependences();
	}

    /**
     * @param array $arParams
     * @return bool
     * @throws Exception
     */
    public function InstallDB($arParams = array())
    {
		global $DB;
		$DB->RunSQLBatch($this->currentDirPath()."/db/mysql/install.sql");
		return true;
	}

	function UnInstallDB($arParams = array())
	{
		global $DB;
		$DB->RunSQLBatch($this->currentDirPath()."/db/mysql/uninstall.sql");
		return true;
	}


	/**
	 * @param array $arParams
	 * @return bool|void
	 * @throws Exception
	 */
	function InstallFiles($arParams = array())
	{
		\Dse\DynamicSnippetsModule::getInstance()->getFileInstaller()->installFiles();
		return true;
	}

	/**
	 * @return bool|void
	 * @throws Exception
	 */
	function UnInstallFiles()
	{
		\Dse\DynamicSnippetsModule::getInstance()->getFileInstaller()->unInstallFiles();
		return true;
	}

	/**
	 * @throws Exception
	 */
	public function DoInstall()
	{
		$this->InstallFiles();
		$this->InstallDB();
		$this->InstallEvents();
		RegisterModule($this->MODULE_ID);
	}

	public function DoUninstall()
	{
		/*global $APPLICATION;
		$APPLICATION->includeAdminFile(
			Loc::getMessage('LANDING_UNINSTALL_TITLE'),
			$_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/'.self::MODULE_ID.'/install/steps/unstep1.php'
		);*/
		$this->UnInstallEvents();
		UnRegisterModule($this->MODULE_ID);
		$this->UnInstallDB();
		$this->UnInstallFiles();
	}

	private function includeAutoload()
	{
	    echo 'includeautoload';
		require_once dirname(__FILE__).'/../include.php';
	}
}
?>
