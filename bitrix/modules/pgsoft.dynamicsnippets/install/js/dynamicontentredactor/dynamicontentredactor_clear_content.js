var ExportVote = {

    step : 3,
    curStep : 0,
    cur_count : 0,
    page : 1,
    load : true,
    allCount : 0,
    exportVoteLoad : function (){
        $('.adm-btn-load-img-green').hide();
        $.ajax({
            type: "POST",
            url: '/bitrix/tools/dynamicontentredactor.php?action=cleancontent&page='+ExportVote.page+'&step='+ExportVote.step,
            data : $('#byte_stats_form').serialize(),
            dataType: 'json',
            success: function(data)
            {
                if(data.all){
                    ExportVote.allCount = data.all;
                }
                if(data.cur_count > 0){
                    ExportVote.page = data.result;
                    var ostatok = ExportVote.allCount - data.cur_count;
                    var percents = ostatok/ExportVote.allCount*100;
                    setTimeout(function() {ExportVote.exportVoteLoad(); });
                    percents--;
                    $( ".adm-progress-bar-inner" ).animate({
                        width: percents+"%",
                    });

                }else{
                    $( ".adm-progress-bar-inner" ).animate({
                        width: "99.2%",
                    });
                }


            }
        });
    },
    exec : function()
    {
        $('#progress_bar').show();
        $('#result').html('');
        this.step = 1;
        this.curStep = 0;
        this.cur_count = 0;
        this.page = 1;
        this.load = true;
        this.allCount = 0;

        $( ".adm-progress-bar-inner" ).width(0)


        ExportVote.exportVoteLoad();
        //	$('#save_stats').hide();
    }
}

