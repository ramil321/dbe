CREATE TABLE `dynamicontentredactor` (
    `id` int NOT NULL AUTO_INCREMENT,
    `active` varchar(1) NOT NULL,
    `name` varchar(20) NOT NULL,
    `iblock_id` int NOT NULL,
    `icon` int NOT NULL,
    `iblock_fields` text NOT NULL,
    `iblock_properties` text NOT NULL,
    `template` text NOT NULL,
    `created_at` datetime NOT NULL,
    `status_at` datetime NOT NULL,
    PRIMARY KEY(`id`)
);
CREATE TABLE `dynamicontentredactor_content` (
    `id` int NOT NULL AUTO_INCREMENT,
    `iblock_id` int NOT NULL,
    `iblock_element_id` int NOT NULL,
    `iblock_field` varchar(20) NOT NULL,
    `iblock_prop` int NOT NULL,
    `dynamicontentredactor_id` int NOT NULL,
    `created_at` datetime NOT NULL,
    PRIMARY KEY(`id`)
);